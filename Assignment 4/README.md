# Shopping Game

This program builds on the Shopping Simulator that we started in Assignment 3.

# New Features
- cart: The cart is now automatically sorted by priority using the bubble sort algorithm

# New Files
All Class files are located in **src/GroceryStore**.
- **GameController.java**
	- This is a interface which is implemented by Shopper.java. It serves as a blueprint for the Controller class that dictates the flow of items between ShoppingCart and Catalog.
- **Item.java**
	- This is an abstract class that is extended by both CatalogItem and CartItem. It enabled me to consolidate redundant access and mutator methods for both Item classes into one abstract class.

## Bug Fixes
I corrected a bug where a non-numerical character or string would be accidentally returned by Scanners nextInt() method. The condition is now caught and handled prior to InputMismatchException being thrown.

## Class Relationships UML
![UML](Assignment 4 UML Diagram.png)