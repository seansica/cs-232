package GroceryStore;

/**
 * @author seansica
 */
public class CatalogItem extends Item{

    /**
     * @attribute inCart, returns true if item is in ShoppingCart or false if not in ShoppingCart
     */
    private boolean inCart;



    /**
     * This is the default constructor
     */
    public CatalogItem(){
        super();
        this.inCart = false;  // default not in cart
    }



    /**
     * This constructor accepts a name for the item as parameter
     * @param itemName This is the name of the item
     */
    public CatalogItem( String itemName ){
        super(itemName);
        this.inCart = false;  // default not in cart
    }



    /**
     * This is an accessor method that determines whether the item is in ShoppingCart
     * @return true, item exists in ShoppingCart
     * @return false; item does not exist in ShoppingCart
     */
    public boolean isInCart(){
        if(this.inCart){
            return false;
        }
        return true;
    }



    /**
     * This is a mutator method that changes the status of whether the specified item exists in the ShoppingCart
     * @param inCart This is a boolean that determines what the items cart status is
     */
    public void setCartStatus(boolean inCart ){
        this.inCart = inCart;
    }

}
