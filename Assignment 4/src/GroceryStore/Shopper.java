package GroceryStore;

import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class implements the GameController abstract class. This is where all game logic occurs as it relates to moving Items between the Catalog and the ShoppingCart.
 */
public class Shopper implements GameController {


    /**
     * @attribute budget, This attribute represents the users monetary budget, default is $59.00
     * @attribute df, This is used to pretty print all monetary values as dollar values rounded to the penny
     * @attribute cart, This attribute is the ShoppingCart object with which the program interacts for the duration of runtime
     */
    private static Double budget;
    private DecimalFormat df = new DecimalFormat("#0.00");
    private ShoppingCart cart = new ShoppingCart();



    /**
     * This is the default constructor for Shopper. It sets the budget to default value $59.00 and invokes the startGame() method.
     */
    Shopper() {
        budget = 59.00;
        this.startGame();
    }



    /**
     * This method invokes an infinite while loop in which the user enters input into a Scanner, which is then evaluated as a parameter in a set of switch block cases.
     * Each switch block case invokes either an accessor and mutator methods that the user can use to interact with the Catalog and ShoppingCart.
     */
    public void startGame() {
    // this method is where all user input occurs. the user never leaves this method.

        // game start: welcome!
        System.out.println("S H O P P I N G   G A M E");
        // instructions
        this.showMenuOptions();

        // user input
        System.out.print("Enter option: ");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();

        // main menu loop
        while (!input.equals("!")) {

            switch (input) {

                case "names":
                    ShoppingGame.store.getNames();
                    break;

                case "prices":
                    ShoppingGame.store.getCosts();
                    break;

                case "stock":
                    ShoppingGame.store.whatsInOutOfStock();
                    break;

                case "catalog":
                    ShoppingGame.store.getCatalog();
                    break;

                case "add":
                    this.addToCart(ShoppingGame.store);
                    break;

                case "cart":
                    this.showCart();
                    break;

                case "remove":
                    this.removeFromCart(cart, ShoppingGame.store);
                    break;

                case "wallet":
                    this.getRunningBalance();
                    break;

                case "checkout":
                    this.checkout();

                    // game over: reset budget
                    this.resetGame();
                    break;
            }
            this.showMenuOptions();
            System.out.print("Enter option: ");
            input = sc.nextLine();
        }
        System.out.println("GAME OVER");
    }



    /**
     * This method is invoked after the user performs a checkout process. It resets the budget to default value, flushes the contents of the ShoppingCart, resets the Catalog contents back to default, and restarts the game.
     */
    public void resetGame() {
        this.resetBudget();
        cart.reset();
        ShoppingGame.store.reset();
    }



    /**
     * This method resets the budget back to default value as defined by global variable MAX_BUDGET
     */
    public void resetBudget() {
        budget = GroceryStoreConstants.MAX_BUDGET;
        System.out.println("Budget reset to $" + df.format(budget) + ": SUCCESS");
    }



    /**
     * This method deducts the passed parameter cost from the remaining budget.
     * @param itemCost, This parameter represents the cost of specified CatalogItem
     */
    public static void setBudget(Double itemCost) {
        budget -= itemCost;
    }



    /**
     * This method gets the remaining budget.
     * @return budget, This return value represents the remaining budget
     */
    public static Double getBudget(){
        return budget;
    }



    /**
     * This method pretty prints the contents of the cart to the console in order of highest priority.
     */
    public void showCart() {
        //Object[] cartItems = cart.getCartItems();

        Object[] sortedItems = this.bubbleSort(cart.getCartItems());

        for (int i = 0; i < sortedItems.length; i++) {

            CartItem cartItem = (CartItem) sortedItems[i];

            String msg = "CartItem{" +
                    "itemName='" + cartItem.getItem().getName() + '\'' +
                    ", priority=" + cartItem.getPriority() +
                    '}';

            System.out.println(msg);
        }
    }



    /**
     * This method sorts an Object array in order of least to highest priority using bubble sort algorithm
     * @param cartItems, This parameter represents the specified list of items that should be sorted.
     * @return This method returns the same array that was originally passed after being sorted
     */
    public Object[] bubbleSort(Object[] cartItems){

        for (int i = 0; i < cartItems.length - 1; i++) {

            for (int j = 0; j < cart.length() - i - 1; j++) {

                CartItem thisItem = (CartItem) cartItems[j];
                CartItem nextItem = (CartItem) cartItems[j+1];

                // if priority of this item is less than priority of the next item
                if(thisItem.getPriority() > nextItem.getPriority()){

                    // then swap this item with the next item
                    Object placeholder = cartItems[j]; // make copy of thisItem
                    cartItems[j] = cartItems[j+1]; // move next item to this items position
                    cartItems[j+1] = placeholder; // move this item to next items position
                }
            }
        }
        return cartItems;
    }



    /**
     * This method pretty prints the game instructions to the console as a reference to the user.
     */
    public void showMenuOptions(){
        System.out.println("OPTIONS:");
        System.out.println("'names'   | displays names of all items in catalog ");
        System.out.println("'prices'  | displays prices of all items in catalog");
        System.out.println("'stock'   | displays availability of all items in catalog");
        System.out.println("'catalog' | displays both name and prices of all items in catalog");
        System.out.println("'cart'    | displays items in shopping cart");
        System.out.println("'wallet'  | displays budget and current running balance");
        System.out.println("'add'     | invokes function to add item to cart");
        System.out.println("'remove'  | invokes function to remove item from cart");
        System.out.println("'checkout'| invokes function to purchase items in shopping cart");
        System.out.println("'!'       | quit program");
    }



    /**
     * This method invokes the checkout process, as follows:
     * 1. In order of highest to least priority value of the CartItems currently in the ShoppingCart,
     *    deduct the cost of the CartItem from the remaining budget, then add the CartItem to a new Array called 'purchasedItems'
     * 2. Continually purchase items until a condition arises such that the cost of the subsequent CartItem is greater than the remaining budget
     * 3. If the aforementioned condition is triggered, do not add the subsequent CartItem to the array of purchased items, and break the loop
     * 4. Print all purchased items and the remaining budget
     */
    public void checkout(){
        System.out.println("Checking out...");
        CartItem[] purchasedItems = cart.checkout( ShoppingGame.store );  // checkout returns CartItem array

        for (int i = 0; i < purchasedItems.length; i++) {
            CartItem purchasedItem = purchasedItems[i];

            /*
            * If we cant buy all items in cart, then purchasedItems will contain null
            * elements. This is because we set purchasedItems equal to the size of the cart.
            * In other words, because we didn't purchase everything in the cart,
            * the cart will contain null elements. The if statement ensures we don't attempt to access
            * null elements.
            */
            if( purchasedItem == null){
                System.out.println("ERROR! No more items can be purchased.");
                break;
            }
            System.out.println("Purchased item: " + purchasedItem.getItem().getName());
        }
        System.out.println("Remaining budget: $" + df.format(budget));

    }



    /**
     * This method pretty prints the remaining budget as calculated by the current budget minus the sum of cost of CartItems currently in the ShoppingCart
     */
    public void getRunningBalance(){
        // used to format cost values down to the penny
        DecimalFormat df = new DecimalFormat("#0.00");

        System.out.println("IN WALLET: $" + df.format(budget) );

        Double runningBalance = cart.currentCostInCart();
        System.out.println("RUNNING BALANCE (WALLET - CART EXPENSES): " + "$" + df.format(runningBalance));

    }



    /**
     * This method attempts to add the specified CatalogItem from Catalog to ShoppingCart
     * @param store, The store indicates from which Catalog the CatalogItem should be referenced
     */
    public void addToCart(Catalog store){
        /**
         * This method has 5 steps:
         * 1. Prompts user for an item name (String) and item priority (Integer)
         * 2. Validates that item exists and is available for purchase.
         * 3. Validates that priority is valid.
         * 3. If item matches an existing CatalogItem in store.catalog (List of CatalogItems),
         *    then instantiate a 'CartItem' object based on the matching CatalogItem and priority
         * 4. Remove the CatalogItem from store.catalog
         * 5. Add the 'CartItem' object to the users shopping cart.
         */

        // init scanner
        Scanner sc = new Scanner(System.in);

        // display what is available
        store.whatsInStock();

        // prompt user to enter item name
        System.out.print("\nPlease enter an item name: ");
        String nameInput = sc.nextLine();

        System.out.print("\nHow important is this item? Items cannot have the same priority." +
                "\nEnter priority [1-7]: ");

        String anyInput = sc.nextLine();  // placeholder

        // determine whether valid priority value entered
        if (!anyInput.toString().matches("[0-9]")) {
            System.out.println("ERROR! Priority must be between 0-9.");
        }
        else{
            // priority value is valid
            Integer priorityInput = Integer.parseInt(anyInput);

            try{
                if (!store.contains(nameInput)) {
                    throw new UnsupportedOperationException("ERROR! " +  nameInput + " is never contained in catalog.");
                }

                // determine whether matching item is in stock
                if (!store.getItem(nameInput).isInCart()) {
                    throw new UnsupportedOperationException("ERROR! Item " + nameInput + " is not available.");
                }


                // if cart already contains item with conflicting priority
                for (int i = 0; i < cart.length(); i++) {
                    CartItem cartItem = (CartItem)cart.getCartItems()[i];
                    if (cartItem.getPriority().equals(priorityInput)){
                        throw new UnsupportedOperationException("ERROR! Priority conflicts with another in cart.");
                    }
                }

                // create item for shopping cart
                CartItem newItem = new CartItem( nameInput, priorityInput, store );

                // add item to cart
                cart.add( newItem, store );

                // remove item from catalog
                store.removeFromStock( newItem.getItem() );

            }
            catch(UnsupportedOperationException | ExceptionInInitializerError | InputMismatchException e){
                System.out.println(e.getMessage());
            }
        }
    }



    /**
     * This method attempts to remove the specified CartItem from ShoppingCart
     * @param cart, The cart represents the ShoppingCart object from which the CartItem should be deducted
     * @param store, The store represents the Catalog to which the CatalogItem contained within the specific CartItem should be returned
     */
    public void removeFromCart( ShoppingCart cart, Catalog store ){
        Scanner sc = new Scanner(System.in);
        System.out.print("Which item would you like to remove from the cart: ");
        String nameInput = sc.nextLine();
        try{
            for (int i = 0; i < cart.length(); i++) {

                // stop if cart contains nothing
                if( cart.length() == GroceryStoreConstants.MIN_CART_SIZE ){
                    throw new UnsupportedOperationException("ERROR! Can't remove any more items.");
                }

                // determine whether the cart contains the item
                CartItem cartItem = (CartItem)cart.getCartItems()[i];
                if( cartItem.getItem().getName().equals(nameInput)){
                    cart.removeFromCart(cartItem, store);
                }
            }
        }
        catch(UnsupportedOperationException e){
            System.out.println(e.getMessage());
        }
    }
}
