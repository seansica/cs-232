package GroceryStore;

public class GroceryStoreConstants {

    public static final Double MAX_BUDGET = 59.00;

    public static final Integer MAX_CART_SIZE = 7;

    public static final Integer MIN_CART_SIZE = 0;

}
