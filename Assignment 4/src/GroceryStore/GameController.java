package GroceryStore;

import java.text.DecimalFormat;

interface GameController {

    /**
     * @attribute budget, This interface requires a default Double of value 59.00 that represents the users budget
     * @attribute df, This interface requires DecimalFormat to pretty print monetary values to the penny
     * @attribute cart, This interface requires a ShoppingCart Object in order for user methods to work
     */
    Double budget = 59.00;
    DecimalFormat df = new DecimalFormat("#0.00");
    ShoppingCart cart = new ShoppingCart();



    /**
     * This method simply prints the available input options that Game is capable of interpreting for the user to reference in the console
     */
    void showMenuOptions();



    /**
     * This method is essentially where the user resides for the duration of the Game.
     * A switch block is embedded within an infinite while loop that accepts user input as parameters
     * for each switch case. Each switch case calls on a method that the user can call to interact with the Catalog and ShoppingCart.
     */
    void startGame();



    /**
     * This method resets the users budget back to default after they complete the checkout process
     */
    void resetBudget();



    /**
     * This method calls on resetBudget() and cart.reset() as a consolidated means to reset all game states back to default
     */
    void resetGame();


    /*
    *
     * This method deducts the cost of an item from the available budget
     * @param itemCost, This parameter represents the cost of the specified item
     */
    //void setBudget(Double itemCost);



    /**
     * This method pretty prints the contents of ShoppingCart; specifically it prints to the console any CartItems and their corresponding attributes
     * @implNote As required by Assignment 4, this method implements Bubble Sort to print the contents of the Cart based on priority
     */
    void showCart();



    /**
     * This method invokes the checkout process, as follows:
     * 1. In order of highest to least priority value of the CartItems currently in the ShoppingCart,
     *    deduct the cost of the CartItem from the remaining budget, then add the CartItem to a new Array called 'purchasedItems'
     * 2. Continually purchase items until a condition arises such that the cost of the subsequent CartItem is greater than the remaining budget
     * 3. If the aforementioned condition is triggered, do not add the subsequent CartItem to the array of purchased items, and break the loop
     * 4. Print all purchased items and the remaining budget
     */
    void checkout();



    /**
     * This method pretty prints the remaining budget as calculated by the current budget minus the sum of cost of CartItems currently in the ShoppingCart
     */
    void getRunningBalance();



    /**
     * This method attempts to add the specified CatalogItem from Catalog to ShoppingCart
     * @param store, The store indicates from which Catalog the CatalogItem should be referenced
     */
    void addToCart(Catalog store);



    /**
     * This method attempts to remove the specified CartItem from ShoppingCart
     * @param cart, The cart represents the ShoppingCart object from which the CartItem should be deducted
     * @param store, The store represents the Catalog to which the CatalogItem contained within the specific CartItem should be returned
     */
    void removeFromCart(ShoppingCart cart, Catalog store);

}
