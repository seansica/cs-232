package GroceryStore;

import java.text.DecimalFormat;

public class Catalog {

    // private attributes - items
    private CatalogItem apple = new CatalogItem( "apple");
    private CatalogItem orange = new CatalogItem("orange");
    private CatalogItem banana = new CatalogItem("banana");
    private CatalogItem kiwi = new CatalogItem("kiwi");
    private CatalogItem avocado = new CatalogItem("avocado");
    private CatalogItem mango = new CatalogItem("mango");
    private CatalogItem blueberry = new CatalogItem("blueberry");
    private CatalogItem watermelon = new CatalogItem("watermelon");
    private CatalogItem melon = new CatalogItem("melon");
    private CatalogItem strawberry = new CatalogItem("strawberry");
    private CatalogItem blackberry = new CatalogItem("blackberry");


    //public attributes - catalog
    private CatalogItem[] catalog = new CatalogItem[11];

    DecimalFormat df = new DecimalFormat("#0.00");

    // constructor
    public Catalog(){

        // populate the catalog
        this.catalog[0] = this.apple;
        this.catalog[1] = this.orange;
        this.catalog[2] = this.banana;
        this.catalog[3] = this.kiwi;
        this.catalog[4] = this.avocado;
        this.catalog[5] = this.mango;
        this.catalog[6] = this.blueberry;
        this.catalog[7] = this.watermelon;
        this.catalog[8] = this.melon;
        this.catalog[9] = this.strawberry;
        this.catalog[10] = this.blackberry;

        // set default attribute values (cost, qty, and status)
        for (int i = 0; i < catalog.length; i++) {
            catalog[i].setCost( 10.00 );  // override default cost to $9.00
            catalog[i].setCartStatus( false );  // override default status to 'not purchased'
        }
    }



    // accessor methods

    public void getNames(){
        for( int i = 0; i < this.catalog.length; i++){

            String currentItem = this.catalog[i].getName();
            System.out.println( "ITEM NAME: " + currentItem );
        }
    }


    public void getCosts(){
        for(int i = 0; i < this.catalog.length; i++){

            String currentItemCost = df.format(this.catalog[i].getCost());
            String currentItemName = this.catalog[i].getName();
            System.out.println( "COST OF " + currentItemName + ": $" + currentItemCost );
        }
    }


    public void whatsInOutOfStock() {
        for (int i = 0; i < this.catalog.length; i++) {
            String currentItemName = this.catalog[i].getName();
            if(this.catalog[i].isInCart()){
                System.out.println( "Item " + currentItemName + " is in stock.");
            }
            else{
                System.out.println("Item " + currentItemName + " is out of stock.");
            }
        }
    }


    public void whatsInStock() {
        for (int i = 0; i < this.catalog.length; i++) {
            String currentItemName = this.catalog[i].getName();
            if(this.catalog[i].isInCart()){
                System.out.println( "Item " + currentItemName + " is in stock.");
            }
        }
    }


    public void getCatalog(){
        for (int i = 0; i < this.catalog.length; i++) {
            String currentItemCost = df.format(this.catalog[i].getCost());
            String currentItemName = this.catalog[i].getName();
            boolean currentItemStatus = this.catalog[i].isInCart();
            System.out.println("Item " + currentItemName + ", Cost $" + currentItemCost + ", In Stock=" + currentItemStatus);
        }
    }


    public boolean contains( CatalogItem compareToItem ){
        /*
        * Purpose: Determines if the store contains the CatalogItem which is passed in the method parameters
         */
        for (int i = 0; i < this.catalog.length; i++) {

            // item in stock: return true
            if ( this.catalog[i].getName().toLowerCase().equals( compareToItem.getName().toLowerCase() )){
                return true;
            }
        }
        // no match found: return false
        return false;
    }


    public boolean contains( String compareToItem ){
        /*
         * Purpose: Determines if the store contains a CatalogItem by searching the catalog for corresponding String CatalogItem.name
         */
        for (int i = 0; i < this.catalog.length; i++) {

            if( this.catalog[i].getName().toLowerCase().equals( compareToItem.toLowerCase() )){
                return true;
            }
        }
        return false;
    }


    public void removeFromStock( CatalogItem itemName ){
        for (int i = 0; i < catalog.length; i++) {
            if( catalog[i].getName().toLowerCase().equals(itemName.getName().toLowerCase()) ){
                catalog[i].setCartStatus( true );
                System.out.println("\nItem " + catalog[i].getName() + " removed from stock!");
            }
        }
    }


    public void returnToStock( CartItem returnItem ){

        // add item back to catalog stock
        for (int i = 0; i < this.catalog.length; i++) {

            // match
            if( this.catalog[i].getName().equals( returnItem.getItem().getName() )){

                this.catalog[i].setCartStatus( false );  // set availability to "not purchased"
            }
        }
    }


    public void returnToStock( String returnItem ){

        // add item back to catalog stock
        for (int i = 0; i < this.catalog.length; i++) {

            // match
            if( this.catalog[i].getName().equals( returnItem )){

                this.catalog[i].setCartStatus( false );  // set availability to "not purchased"
            }
        }
    }


    public CatalogItem getItem( String itemName ){

        for (int i = 0; i < this.catalog.length; i++) {
            if( this.catalog[i].getName().equals(itemName) ){
                return catalog[i];
            }
        }
        throw new UnsupportedOperationException("No Item with Name '" +
                                                itemName + "' found in Catalog.");
    }

    public void reset() {

        // reset catalog values to default
        for (int i = 0; i < this.catalog.length; i++) {
            catalog[i].setCost( 9.00 );  // override default cost to $9.00
            catalog[i].setCartStatus( false );  // override default status to 'not purchased'
        }
        System.out.println("Catalog reset to default: SUCCESS");
    }
}
