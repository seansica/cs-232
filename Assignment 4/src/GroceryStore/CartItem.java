package GroceryStore;

/**
 * @author seansica
 * @implNote CartItem implements the Comparable interface to take advantage of its sorting method, compareTo(Object). CartItem overrides Comparables compareTo(Object) method by forcing the method to compare the Integer priority value of two Items
 * @implNote CartItem also extends the Item abstract class. All methods pertaining to itemName, itemPriority are handled in the abstract class
 */
class CartItem extends Item implements Comparable{

    /**
     * @attributes item This is a CatalogItem. CartItem contains a CatalogItem by design and therefore requires a CatalogItem attribute
     */
    private CatalogItem item;



    /**
     * This is the default constructor class for CartItem
     * @param itemName This is the name of the item being added to the cart
     * @param itemPriority This is the user-assigned priority value of the item being added to the cart
     * @param store This is the Catalog from which the item is being purchased
     */
    CartItem(String itemName, Integer itemPriority, Catalog store) {

        super(itemName, itemPriority);
        this.item = store.getItem(itemName);
    }



    /**
     * This is a accessor method used to fetch the Item object of CartItem
     * @return this.item Returns the Item Class object contained within the cart
     */
    public CatalogItem getItem(){
        return this.item;
    }



    /**
     * This is an accessor method used to fetch the cost (Double) of CartItem
     * @return item.cost The cost of the item is a Double that is returned via call to item.getCost()
     * @override This method overrides Item.getCost() because CartItem must make a call to the contained items getCost method; it does not have direct access to the item.cost variable
     */
    @Override
    public Double getCost(){
        return this.item.getCost();
    }



    /**
     * This is a mutator method used to overwrite which type of CatalogItem is associated with the CartItem
     * @param newItem This is the new item from the Catalog being passed to the CartItem object
     */
    public void setItem( CatalogItem newItem ){
        this.item = newItem;
    }



    /**
     * This is an accessor method that determines whether the parameter item is this item
     * @param compareItem This is the item in the shopping cart that is being compared to this item
     * @return true if item is same; false if item is different
     */
    public boolean equals( CartItem compareItem ){

        String thisItemName = this.item.getName().toLowerCase();
        String thatItemName = compareItem.getItem().getName().toLowerCase();

        if( thisItemName.equals( thatItemName ) ){
            return true;
        }
        return false;
    }



    /**
     * This is an accessor method that determines whether the parameter item is this item
     * @param compareItem This is the item in the shopping cart that is being compared to this item
     * @return true, if item is same
     * @return false, if item is different
     * @implNote This overloads the equals(CartItem) method; it is simply another means of comparing items
     */
    public boolean equals( CatalogItem compareItem ){

        String thisItemName = this.item.getName().toLowerCase();
        String thatItemName = compareItem.getName().toLowerCase();

        if( thisItemName.equals( thatItemName ) ){
            return true;
        }

        return false;
    }



    /**
     * This is an accessor method that compares the priority of two items. The purpose is for sorting items in the cart based on priority.
     * @param o This is the item that we are comparing this item to.
     * @return positive integer, if the current object is greater than the specified object.
     * @return negative integer, if the current object is less than the specified object.
     * @return zero, if the current object is equal to the specified object.
     */
    @Override
    public int compareTo(Object o) {
        /*
        * Purpose: Override superclasses compareTo() method because we want to always sort
        *          the PriorityQueue by MyItem.priority integer.
         */
        CartItem other = (CartItem)o;
        return Integer.compare( this.priority, other.getPriority() );
    }



    /**
     * This is an accessor method that pretty prints the attributes of the specified CartItem
     * @return Prints a human-readable String that contains this.itemName and this.priority
     */
    @Override
    public String toString() {
        /*
        * Purpose: Override superclass toString() method to make printing (str)attributes in a meaningful way,
        *          rather than printing the entire MyItem object in memory.
         */
        return "MyItem{" +
                "itemName='" + itemName + '\'' +
                ", priority=" + priority +
                '}';
    }
}
