package GroceryStore;

/**
 * @author seansica
 */
abstract class Item {

    /**
     * @attribute itemName, This is the name of the specified Item
     * @attribute priority, This is the shopping list priority number [0-7] of the specified Item
     * @attribute cost, This is the cost of the specified Item as defined in the Catalog
     */
    String itemName;
    Integer priority;
    Double cost;



    /**
     * This is the default constructor for Item. It defines a unusable name, priority, and cost
     */
    Item(){
        this.itemName = "";
        this.priority = 0;
        Double cost = 0.0;
    }



    /**
     * This is a constructor that accepts item name as parameter
     * @param itemName This is the name of the Item
     */
    Item(String itemName){
        super();
        this.itemName = itemName;
    }



    /**
     * This is a constructor that accepts item name and priority as parameters
     * @param itemName This is the name of the item
     * @param itemPriority This is the shopping list priority of the item
     */
    Item(String itemName, Integer itemPriority){
        super();
        this.itemName = itemName;
        this.priority = itemPriority;
    }



    /**
     * This is a constructor that accepts item name, priority, and cost as parameters
     * @param itemName This is the name of the item
     * @param itemPriority This is the shopping list priority of the item
     * @param itemCost This is the cost of the item as defined in the Catalog
     */
    Item(String itemName, Integer itemPriority, Double itemCost){
        this.itemName = itemName;
        this.priority = itemPriority;
        this.cost = itemCost;
    }



    /**
     * This is an accessor method that gets the name of the Item
     * @return itemName This is the name of the item
     */
    public String getName(){
        return this.itemName;
    }



    /**
     * This is an accessor method that gets the priority of the Item
     * @return priority This is the user-defined shopping list priority number [0-7] of the Item
     */
    public Integer getPriority(){
        return this.priority;
    }



    /**
     * This is an accessor method used to fetch the cost (Double) of Item
     * @return item.cost This is the cost of the item as a Double
     */
    public Double getCost(){
        return this.cost;
    }



    /**
     * This is a mutator method that sets the name of the Item
     * @param newName This is the new name (String) of the Item
     */
    public void setItemName(String newName){
        this.itemName = newName;
    }



    /**
     * This is a mutator method used to overwrite the user-assigned priority of Item
     * @param newPriority This is the new user-assigned priority value of Item
     */
    public void setPriority(Integer newPriority){
        this.priority = newPriority;
    }



    /**
     * This is a mutator method that sets the cost of the Item
     * @param newCost This is the new cost of the Item
     */
    public void setCost(Double newCost){
        this.cost = newCost;
    }
}
