# Report Builder

In summary, this program simply allows users to record input in the console, output calculations, and record data. It does not leverage the ArrayList Class, only primitive arrays. The goal of this exercise is to get acquainted with primitives, basic Classes, and object-oriented code design.

# Features
- Enter and Record **First Name(s)**. All First Names are recorded until program exit.
- Enter and Record **Last Name(s)** _one character at a time_. All Last Names are recorded until program exit.
- Enter and Record "Report Values" (7-Digit Numbers). All Report Values are recorded until program exit.
- Calculate **Average** of **Current Report Value**
	- _i.e._ If user enters "1234567", then calculate average of [1,2,3,4,5,6,7]
- Calculate **Average** of **All Recorded Report Values**
	- _i.e._ If user enters 3 Report Values -- 1111111, 2222222, and 3333333 -- then calculate average of [1111111, 2222222, 3333333]
- Calculate **Lowest** Integer Index for **Current Report Value**
- Calculate **Highest** Integer Index for **Current Report Value**
- Calculate **Lowest** Integer Index for **All Recorded Report Values**
- Calculate **Highest** Integer Index for **All Recorded Report Values**
- Calculate **Sum** of **Current Report Value**
- Calculate **Sum** of **All Recorded Report Values**

# Files
All Class files are located in **src/Report**.
- **Report.java**
	- This is where Main and our static variables reside. It is the starting point for the program.
- **User.java**
	- This Class file is where all of the methods, variable manipulation, and user interaction occurs. It instantiates a Tools Class object called _toolbox_ to alleviate some of the pain with handling primitive arrays.
- **Tools.java**
	- This Class file stores various methods that I found particularly useful for manipulating primitive arrays and calculating integer logic.


## Limitations

At this time, there are no known bugs that will cause the program to crash. In addition, all known Class & Primitive limitations have been considered and caught within the code. For example, the program uses _integers_ very frequently for calculating number logic (such as avg, min., max., etc.), therefore the maximum-allowed integer value was set to 7.

## Pseudo Code
    **************
    *Report.java*
    **************
    Main:
        Instantiate User object from User Class
        call User.mainMenu()
    
    
    **************
    **User.java**
    **************
    Instantiate Tools object from Tools Class called "toolbox"

    mainMenu()
        prompt user to enter report number

        while(true) -> infinite loop

            switch: -> give user menu options
                "stats" -> calculate sum, count, and avg of current number
                "first" -> prompt for and save first name
                "last" -> prompt for and save last name
                "sum" -> find sum of all ints in current number
                "minmax" -> find min and max values of current number
                "report" -> run report on current number and run report on collection of all report numbers
                "new" -> add new report number
                "!" -> end program

    private setFirstName()
        while true
            get scanner input
            if scanner input contains break character: break
            else: sanitize scanner variable + add to firstName variable

    private setLastName()
        create primitive array of arbitrary size
        get scanner input -> char
        evaluate char:
            if char matches(A-Za-z): add char to array
            if char contains break char or if loop has reached end: stop, convert array to String, trim off empty elements, and return variable

    private setReportName()
        while (true):
            try: get scanner input -> int
                if input is valid: save input and return
                else try again
            catch: invalid input, try again

    private getStats()
        try:
            if: report number is not valid: break
            else:
                convert report number to array -> toolbox.split()
                then calculate stats (avg,sum,count) on array
                and set variables
        catch: report number not valid, try again

    private getSum()
        convert report number to array -> toolbox.split()
        sum all ints in array
        set variable

    private getMinMax()
        convert report number to array -> toolbox.split()

    private grandTotal()
        Run stats on collection of all report numbers
        Show stats for current number
        Show stats for collection of all report numbers

    **************
    **Tools.java**
    **************
  
    public appendArray( array, String or Int)
        convert array into new array of size++
        copy elements from old to new array
        push String or Int into new array

    public sumInts( array )
        for e in array: sum += array[e]

    pubic split( string or number )
        convert report number to string
        create array
        for e in array: array[e] = each char of string

    public getMinMax( array )
        set highest to first element of array
        set lowest to first element of array

        compare first element of array to each subsequent element in array
        if we reach an element that is higher than highest: set that element as the new highest
        if we reach an element that is lower than lowest: set that element as the lowest

        store highest and lowest in array and return that

    public getStats( array )
        sum = this.getSum()
        count = array.length
        avg = sum/count

        store sum, count, and avg into array and return that

    public printArray (int array or string array)
        for e in array: print array[e]


## Lessons Learned
In hindsight, I think the overall organization of this code is confusing and a bit redundant. Further work could be done to consolidate the code into more modular methods.

In addition, some naming convention overlap may cause some confusion. For example, nearly-identical method **getStats()** exists in both **User.java** and **Tools.java**. The reason this happened was because, originally, the code was only designed to perform logic on singular, disposable 7-digit integers. Therefore the methods within **User.java** were sufficient at the time to get the job done. When I decided to eventually expand the code to additionally allow for integer logic to be performed on recorded data sets (not just 7-digit ints, one at a time), I needed to obfuscate the logic methods (i.e. the methods performing the logic) from the methods actually executing/calling the logic. Next time I will do some better planning.


## Class Relationships UML
![UML](CS-232%20Assignment%202%20-%20UML.png)