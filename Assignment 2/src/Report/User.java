package Report;

import java.util.Scanner;

public class User {

    // instantiate methods to simplify primitive logic
    Tools toolbox = new Tools();

    public void mainMenu(){
        System.out.println("I see you're new here. Let's get started...");
        this.setReportName();  // initial setup

        String instructions = "* * * E P I C  C O N S O L E   G A M E * * *\nWelcome, adventurer!" +
                "\nEnter 'first' to enter your LAST Name." +
                "\nEnter 'last' to enter your FIRST Name." +
                "\nEnter 'stats' to calculate the AVERAGE, SUM, and COUNT for Current Report Value." +
                "\nEnter 'sum' to calculate SUM of Current Report Value." +
                "\nEnter 'minmax' to calculate HIGHEST and LOWEST index values for Current Report Value." +
                "\nEnter 'new' for NEW Report Value." +
                "\nEnter 'report' to SUMMARIZE all recorded data thus far." +
                "\nEnter '!' to Quit." +
                "\nEnter Option: ";

        System.out.print(instructions);
        Scanner menuListener = new Scanner(System.in);
        String selectedOption = menuListener.nextLine();

        outer:
        while( true ){

            inner:
            switch( selectedOption ){
                case "stats" :
                    this.getStats();
                    break inner;
                case "first" :
                    this.setFirstName();
                    break inner;
                case "last" :
                    this.setLastName();
                    break inner;
                case "sum" :
                    this.getSum();
                    break inner;
                case "minmax" :
                    this.getMinMax();
                    break inner;
                case "report" :
                    this.grandTotal();
                    break inner;
                case "new" :
                    this.setReportName();
                    break inner;
                case "!" :
                    break outer;
            }
            System.out.print("\nYou have returned to the Main Menu!\n" + instructions);
            selectedOption = menuListener.nextLine();
        }
    }


    private String setFirstName() {
        Scanner fnScanner = new Scanner(System.in);  // create scanner to collect user input
        System.out.print("Please enter your first name: ");
        Report.firstName = fnScanner.nextLine(); // prompt user for input
        while( true ){
            if( Report.firstName.contains("!") ){
                System.out.println( "Fine! Keep your secrets." );
            }
            else{
                // remove special characters from input via regex
                Report.firstName = Report.firstName.replaceAll("[^a-zA-Z]", "");
                System.out.println("Hi, " + Report.firstName + "." + "\n");
                Report.allRecordedFirstNames = toolbox.appendArray(Report.allRecordedFirstNames, Report.firstName); // record first name
                return Report.firstName;
            }
        }
    }



    private String setLastName() {
        int i = 0;  // index
        char[] tempLastName = new char[32];  // initialize character array of arbitrary length 32
        Scanner lnScanner = new Scanner(System.in); // create scanner to collect user input

        System.out.println("\nEnter each letter of your last name on a separate line.\n" +
                        "If you enter multiple characters on one line, only the first character will be accepted.\n" +
                        "When you are done, enter exclamation point(!)\n" );

        System.out.println("What is your last name?");

        // loop up to 31 times; 32 was arbitrarily selected as the max character ct. for last name
        while ( true ){
            // ACTION: prompt user for input
            System.out.print("Enter letter: ");
            Character input = lnScanner.next().charAt(0);  // capture only the first character that user inputs
            String evalString = input.toString();   // store input in String for evaluation

            // MATCH: if input is any alpha numeric letter A-Z or a-z
            if ( evalString.matches("[A-Za-z]") ){
                System.out.println("Adding char " + input + " to String Array.");
                tempLastName[i++] = input;  // add scanner input to array
            }

            // BREAK: user enters ! or loop index reaches max character limit
            if ( evalString.contains("!") || i == 31 ){
                Report.lastName = new String(tempLastName).trim();  // convert lastName char array into String and trim off whitespace
                Report.allRecordedLastNames = toolbox.appendArray(Report.allRecordedLastNames, Report.lastName); // record last name
                System.out.print("\n Last Name Received!\n Last Name: " + Report.lastName);
                return Report.lastName;
            }
        }
    }



    private int setReportName() {
        System.out.println("Your current report value is: " + Report.reportNumber);
        Scanner reportScanner = new Scanner(System.in);  // create scanner to collect user input
        System.out.println("\n\nReport Name may only contains numbers and must be seven digits long.");

        while( true ){
            try{
                System.out.print("Enter Report Name [7-char max]: ");
                Report.reportNumber = reportScanner.nextInt();
                reportScanner.nextLine();  // throw away the \n not consumed by nextInt()

                // number must not be negative
                if( Report.reportNumber >= 0 ){

                    // number must contain 7 digits
                    if( String.valueOf(Report.reportNumber).length() == 7 ){
                        System.out.println("Valid Report Name Received!");
                        Report.allRecordedReports = toolbox.appendIntArray(Report.allRecordedReports, Report.reportNumber);  // record report number in array
                        return Report.reportNumber;
                    }else{
                        System.out.println("Please enter a 7-digit numeric value only!");
                    }
                }
            }
            catch( java.util.InputMismatchException e ){
                System.out.println("ERROR: Invalid Input. Please enter a 7-digit numeric value only!");
                reportScanner.nextLine();
            }
        }
    }



    private double getStats(){
        try{
            // Do not evaluate input > 10 characters due to limitation of primitive (int)
            if( String.valueOf(Report.reportNumber).length() > 9 ){
                System.out.println("Maximum allowed numbers is 9. Please try again.");
                return 0;
            }
            else{
                int[] numArray = toolbox.split( Report.reportNumber );  // convert int to array of int-chars
                double[] stats = toolbox.getStats( numArray ); // calculate stats on array elements
                Report.sum = (int)stats[0];
                Report.intCount = (int)stats[1];
                Report.avg = stats[2];

                // print results
                System.out.println("Report Number: " + Report.reportNumber);
                System.out.println("SUM: " + Report.sum);
                System.out.println("COUNT: " + Report.intCount);
                System.out.println("AVG: " + Report.avg);
            }
        }
        catch( java.util.InputMismatchException e ){
            System.out.println("ERROR! Only numbers are valid input. Please try again.");
        }
        return Report.avg;
    }



    private void getSum(){
        int[] numArray = toolbox.split( Report.reportNumber );  // convert int to array of int-chars
        Report.sum = toolbox.sumInts( numArray );  // sum all in array
        Report.intCount = numArray.length; // get length of report
        System.out.println("COUNT: " + Report.intCount);
        System.out.println("SUM: " + Report.sum);
    }



    private int[] getMinMax(){
        int[] numArray = toolbox.split( Report.reportNumber );  // convert int to array of int-chars
        int[] minmax = toolbox.getMinMax( numArray );
        Report.lowest = minmax[0];
        Report.highest = minmax[1];

        System.out.println("LOWEST Value in Report: " + Report.lowest);
        System.out.println("HIGHEST Value in Report: " + Report.highest);
        return minmax;
    }



    private void grandTotal(){
        // calculate stats for current values
        Report.lowest = toolbox.getMinMax( toolbox.split(Report.reportNumber))[0];
        Report.highest = toolbox.getMinMax( toolbox.split(Report.reportNumber))[1];

        // calculate stats for all recorded values
        double[] stats = toolbox.getStats( Report.allRecordedReports );
        int sum = (int)stats[0];
        int intCount = (int)stats[1];
        double avg = stats[2];
        int[] minmax = toolbox.getMinMax( Report.allRecordedReports );
        int min = minmax[0];
        int max = minmax[1];

        // show all recorded first and last names
        System.out.print("All Recorded First Names: ");
            toolbox.printStrArray(Report.allRecordedFirstNames);
        System.out.print("All Recorded Last Names: ");
            toolbox.printStrArray(Report.allRecordedLastNames);

        // show stats for current report value
        System.out.println("CURRENT REPORT VALUE: " + Report.reportNumber);
        System.out.println("CURRENT VALUE - SUM: " + Report.sum);
        System.out.println("CURRENT VALUE - COUNT: " + Report.intCount);
        System.out.println("CURRENT VALUE - AVG: " + Report.avg);
        System.out.println("CURRENT VALUE - MIN: " + Report.lowest );
        System.out.println("CURRENT VALUE - MAX: " + Report.highest );

        // show stats for all recorded report values
        System.out.print("\nAll Recorded Reports: ");
            toolbox.printIntArray(Report.allRecordedReports);
        System.out.println("ALL RECORDED - SUM: " + sum);
        System.out.println("ALL RECORDED - COUNT: " + intCount);
        System.out.println("ALL RECORDED - AVG: " + avg);
        System.out.println("ALL RECORDED - MIN: " + min);
        System.out.println("ALL RECORDED - MAX: " + max);
    }

}

