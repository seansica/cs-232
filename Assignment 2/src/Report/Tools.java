package Report;

public class Tools {


    // method to simplify looping over primitive String array
    // used to append firstName and lastName to allRecordedFirstNames and allRecordedLastNames
    public String[] appendArray( String[] array, String x ){
        String[] newArray = new String[array.length + 1];

        for(int i = 0; i < array.length; i++){
            newArray[i] = array[i];
        }

        newArray[newArray.length - 1] = x;
        return newArray;
    }


    // method to simplify looping over primitive int array
    // used to append global int vars to allRecorded__Int__Names (EXAMPLE: add reportNumber to allRecordedReportNumbers)
    public int[] appendIntArray( int[] array, int x ){

        int[] newArray = new int[array.length + 1];  // create new array +1 size up

        for(int i = 0; i < array.length; i++){
            newArray[i] = array[i];   // copy ints from old to new array
        }

        newArray[newArray.length - 1] = x;
        return newArray;
    }


    // method to sum all ints in array
    public int sumInts( int[] array ){
        int sum = 0; // reset sum

        for( int i = 0; i < array.length; i++ ){
            sum += array[i];  // numerator
        }
        return sum;
    }

    // method to split primitive int into array of primitive ints
    public int[] split( int number ){
        String string = Integer.toString(number);  // convert reportNumber to String
        int[] array = new int[string.length()];  // create int array

        // populate the array with each int char reportNumber
        for( int i = 0; i < string.length(); i++ ){
            if( String.valueOf(i).matches("[0-9]")){
                array[i] = string.charAt(i) - '0';
            }
        }
        return array;
    }

    // method to split String array into array of strings
    public int[] split( String string ){
        int[] array = new int[string.length()];  // create int array

        // populate the array with each int char reportNumber
        for( int i = 0; i < string.length(); i++ ){
            if( String.valueOf(i).matches("[0-9]")){
                array[i] = string.charAt(i) - '0';
            }
        }
        return array;
    }

    public int[] getMinMax( int[] array ){
        int currentHighest = array[0];
        int currentLowest = array[0];

        // evaluate each number in array starting with the first index
        for( int i = 0; i < array.length; i++ ){

            // next number is higher than current number; found new highest
            if( currentHighest < array[i] ){
                // System.out.println("Found new highest! Number at index " + (i) + " is new highest!");
                currentHighest = array[i];
            }

            if( currentLowest > array[i] ){
                // System.out.println("Found new lowest! Number at index " + (i) + " is new lowest!");
                currentLowest = array[i];
            }
        }
        // set static vars
        int highest = currentHighest;
        int lowest = currentLowest;

        // add static vars to array
        int[] minMax = new int[2];
        minMax[0] = lowest;
        minMax[1] = highest;

        return minMax;
    }

    public double[] getStats(int[] array ){
        double[] stats = new double[3];
        int sum = this.sumInts( array );  // numerator
        double intCount = array.length; // denominator
        double avg = (sum/intCount);  // calculate average

        stats[0] = sum;
        stats[1] = intCount;
        stats[2] = avg;
        return stats;
    }

    public void printIntArray( int[] array ){
        for( int i = 0; i < array.length; i++){
            System.out.print( array[i] + ", ");
        }
        System.out.println();
    }

    public void printStrArray( String[] array ){
        for( int i = 0; i < array.length; i++){
            System.out.print( array[i] + ", " );
        }
        System.out.println();
    }

}
