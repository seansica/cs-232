/**
 * Author: Sean Sica
 * Date: 24 September 2019
 *
 * CS-232 Programming with Java
 * Assignment 2: Build A Report
 * BU MET
 *
 **/

package Report;

public class Report {

    // attributes

    // ** current ** report values
    public static int reportNumber;
    public static String firstName;
    public static String lastName;
    public static double avg;
    public static int highest;
    public static int lowest;
    public static int sum;
    public static int intCount;

    // ** all recorded ** report values
    public static int[] allRecordedReports = new int[0];
    public static String[] allRecordedFirstNames = new String[0];
    public static String[] allRecordedLastNames = new String[0];

    // MAIN
    public static void main( String[] args ){

        User player1 = new User();  // instantiate user
        player1.mainMenu();  // execute game
    }
}