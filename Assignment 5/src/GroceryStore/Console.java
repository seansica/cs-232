package GroceryStore;


import java.io.*;
import java.text.SimpleDateFormat;

/**
 * The purpose of this abstract class is to centrally handle all console printing.
 */
abstract class Console {

    private static final String ANSI_RED = "\u001B[31m";

    private static final String ANSI_BLUE = "\u001B[34m";

    private static final String ANSI_RESET = "\u001B[0m";



    /**
     * This method is intended to print DEBUG messages to the console. They are highlighted in blue
     * It can be turned off by setting global variable SYSLOG_DEBUG to false
     * @param msg This is the message that will be printed to console
     */
    public static void debug(String msg){

        if(GroceryStoreConstants.SYSLOG_DEBUG){

            System.out.println(ANSI_BLUE + "DEBUG: " + msg + ANSI_RESET);

            LogHandler(msg);

        }
        return;
    }



    /**
     * This method is intended to print pertinent program-functionality-related messages to the console. It appends a newline to the end of the message
     * @param msg This is the message that will be printed to console
     */
    public static void println(String msg){
        System.out.println(msg);
    }



    /**
     * This method is intended to print pertinent program-functionality-related messages to the console. It does NOT append a newline to the end of the message
     * @param msg This is the message that will be printed to console
     */
    public static void print(String msg){
        System.out.print(msg);
    }



    /**
     * This method is intended to print ERROR messages to the console. They are highlighted in red
     * It can be turned off by setting global variable SYSLOG_ERROR to false
     * @param msg This is the message that will be printed to console
     */
    public static void error(String msg){

        if(GroceryStoreConstants.SYSLOG_ERROR){

            System.out.println(ANSI_RED + "ERROR! " + msg + ANSI_RESET);

            LogHandler(msg);
        }
        return;
    }


    /**
     * This is a mutator method that appends all DEBUG and ERROR messages to a log file. The path and filename of the log is defined by GroceryStoreConstants.LOGGING_FILEPATH.
     * All log messages are accompanied by a corresponding timestamp.
     * @param msg This is the String which should be appended to the log
     */
    private static void LogHandler(String msg){

        // Initialize timestamp handler
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        // Initialize writer placeholder
        Writer writer = null;

        try{
            // Open the file in write mode
            writer = new BufferedWriter(new FileWriter(GroceryStoreConstants.LOGGING_FILEPATH, true));

            // Append timestamp and debug/error message to log file
            writer.write(timeStamp + " : " + msg + "\n");

            // Close the file when complete
            writer.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }

}