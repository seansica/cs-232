package GroceryStore;

/**
 * The purpose of this custom Exception class is to handle conditions in which the quantity of CatalogItems contained
 * in ShoppingCart is less than zero; there can never be negative stock.
 */
public class UnavailableItemException extends Exception{

    /**
     * This is the constructor; it simply passes a parameter String to the Exception class constructor.
     * @param msg This parameter refers to a user-defined String. It is suggested that the message
     *            elaborate why negative stock is not supported.
     */
    public UnavailableItemException(String msg){
        Console.error(msg);
        super.printStackTrace();
    }
}
