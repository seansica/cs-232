package GroceryStore;

/**
 * @author seansica
 */
abstract class Item {

    /**
     * @attribute itemName, This is the name of the specified Item
     * @attribute priority, This is the shopping list priority number [0-7] of the specified Item
     * @attribute cost, This is the cost of the specified Item as defined in the Catalog
     */
    protected String name;
    protected Integer priority;
    protected Double cost;
    protected Integer qty;



    /**
     * This is the default constructor for Item. It defines a unusable name, priority, and cost
     */
    public Item(){
        this.name = "";
        this.priority = 0;
        this.cost = 0.0;
        this.qty = 1;
    }



    /**
     * This is a constructor that accepts item name as parameter
     * @param name This is the name of the Item
     */
    public Item(String name){
        super();
        this.name = name;
    }



    /**
     * This is a constructor that accepts item name and priority as parameters
     * @param name This is the name of the item
     * @param itemPriority This is the shopping list priority of the item
     */
    public Item(String name, Integer itemPriority){
        super();
        this.name = name;
        this.priority = itemPriority;
    }

    public Item(String name, Integer priority, Integer qty) {
        super();
        this.name = name;
        this.priority = priority;
        this.qty = qty;
    }

    public Item(String name, Integer priority, Integer qty, Double cost) {
        super();
        this.name = name;
        this.priority = priority;
        this.qty = qty;
        this.cost = cost;
    }


//    /**
//     * This is a constructor that accepts item name, priority, and cost as parameters
//     * @param name This is the name of the item
//     * @param priority This is the shopping list priority of the item
//     * @param cost This is the cost of the item as defined in the Catalog
//     */
//    Item(String name, Integer priority, Double cost){
//        super();
//        this.name = name;
//        this.priority = priority;
//        this.cost = cost;
//    }



//    /**
//     * This is a constructor that accepts item name, priority, cost , and quantity as parameters
//     * @param name This is the name of the item
//     * @param priority This is the shopping list priority of the item
//     * @param cost This is the cost of the item as defined in the Catalog
//     * @param qty This is the quantity of the Item
//     */
//    Item(String name, Integer priority, Integer qty){
//        super();
//        this.name = name;
//        this.priority = priority;
//        this.qty = qty;
//    }



    /**
     * This is an accessor method that gets the name of the Item
     * @return itemName This is the name of the item
     */
    public String getName(){
        return this.name;
    }



    /**
     * This is an accessor method that gets the priority of the Item
     * @return priority This is the user-defined shopping list priority number [0-7] of the Item
     */
    public Integer getPriority(){
        return this.priority;
    }



    /**
     * This is an accessor method used to fetch the cost (Double) of Item
     * @return item.cost This is the cost of the item as a Double
     */
    public Double getCost(){
        return this.cost;
    }



    /**
     * This is an accessor method that gets the qty of the specified Item in stock.
     * @return Returns an Integer that represents the quantity of the specified Item in stock
     */
    public Integer getQty(){
        return this.qty;
    }



    /**
     * This is a mutator method that sets the name of the Item
     * @param newName This is the new name (String) of the Item
     */
    public void setName(String newName){
        this.name = newName;
    }



    /**
     * This is a mutator method used to overwrite the user-assigned priority of Item
     * @param newPriority This is the new user-assigned priority value of Item
     */
    public void setPriority(Integer newPriority){
        this.priority = newPriority;
    }



    /**
     * This is a mutator method that sets the cost of the Item
     * @param newCost This is the new cost of the Item
     */
    public void setCost(Double newCost){
        this.cost = newCost;
    }



    /**
     * This is a mutator method that sets the quantity of the Item in stock
     * @param newQty This is the specified quantity that should be set
     */
    public void setQty(Integer newQty){
        this.qty = newQty;
    }
}
