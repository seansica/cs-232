package GroceryStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * This class implements the GameController abstract class. This is where all game logic occurs as it relates to moving Items between the Catalog and the ShoppingCart.
 */
public class Shopper implements GameController {


    /**
     * @attribute budget, This attribute represents the users monetary budget, default is $59.00
     * @attribute df, This is used to pretty println all monetary values as dollar values rounded to the penny
     * @attribute cart, This attribute is the ShoppingCart object with which the program interacts for the duration of runtime
     */
    private static Double budget;
    private DecimalFormat df = new DecimalFormat("#0.00");
    private ShoppingCart cart = new ShoppingCart();



    /**
     * This is the default constructor for Shopper. It sets the budget to default value $59.00 and invokes the startGame() method.
     */
    Shopper() {
        budget = 59.00;
        this.startGame();
    }



    /**
     * This method invokes an infinite while loop in which the user enters input into a Scanner, which is then evaluated as a parameter in a set of switch block cases.
     * Each switch block case invokes either an accessor and mutator methods that the user can use to interact with the Catalog and ShoppingCart.
     */
    public void startGame() {
    // this method is where all user input occurs. the user never leaves this method.

        // game start: welcome!
        Console.println("S H O P P I N G   G A M E");
        // instructions
        this.showMenuOptions();

        // user input
        Console.println("Enter option: ");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();

        // main menu loop
        while (!input.equals("!")) {

            switch (input) {

                case "names":
                    ShoppingGame.store.getNames();
                    break;

                case "prices":
                    ShoppingGame.store.getCosts();
                    break;

                case "stock":
                    ShoppingGame.store.whatsInOutOfStock();
                    break;

                case "catalog":
                    ShoppingGame.store.getCatalog();
                    break;

                case "add":
                    this.addDecisionTree();
                    break;

                case "cart":
                    this.showCart();
                    break;

                case "remove":
                    this.removeFromCart(cart, ShoppingGame.store);
                    break;

                case "wallet":
                    this.getRunningBalance();
                    break;

                case "checkout":
                    this.checkout();

                    // game over: reset budget
                    this.resetGame();
                    break;
            }
            this.showMenuOptions();
            Console.println("Enter option: ");
            input = sc.nextLine();
        }
        Console.println("GAME OVER");
    }



    /**
     * This method is invoked after the user performs a checkout process. It resets the budget to default value, flushes the contents of the ShoppingCart, resets the Catalog contents back to default, and restarts the game.
     */
    public void resetGame() {
        this.resetBudget();
        cart.reset();
        ShoppingGame.store.reset();
    }



    /**
     * This method resets the budget back to default value as defined by global variable MAX_BUDGET
     */
    public void resetBudget() {
        budget = GroceryStoreConstants.MAX_BUDGET;
        Console.debug("Budget reset to $" + df.format(budget) + ": SUCCESS");
    }



    /**
     * This method deducts the passed parameter cost from the remaining budget.
     * @param itemCost, This parameter represents the cost of specified CatalogItem
     */
    public static void setBudget(Double itemCost) {
        budget -= itemCost;
    }



    /**
     * This method gets the remaining budget.
     * @return budget, This return value represents the remaining budget
     */
    public static Double getBudget(){
        return budget;
    }



    /**
     * This method pretty prints the contents of the cart to the console in order of highest priority.
     */
    public void showCart() {
        //Object[] cartItems = cart.getCartItems();

        Object[] sortedItems = this.bubbleSort(cart.getCartItems());

        Console.println("IN CART:");
        for (int i = 0; i < sortedItems.length; i++) {

            CartItem cartItem = (CartItem) sortedItems[i];

            String msg = "CartItem{" +
                    "itemName='" + cartItem.getName() + '\'' +
                    ", priority=" + cartItem.getPriority() +
                    '}';

            Console.println(msg);
        }
    }



    /**
     * This method sorts an Object array in order of least to highest priority using bubble sort algorithm
     * @param cartItems, This parameter represents the specified list of items that should be sorted.
     * @return This method returns the same array that was originally passed after being sorted
     */
    public Object[] bubbleSort(Object[] cartItems){

        for (int i = 0; i < cartItems.length - 1; i++) {

            for (int j = 0; j < cart.length() - i - 1; j++) {

                CartItem thisItem = (CartItem) cartItems[j];
                CartItem nextItem = (CartItem) cartItems[j+1];

                // if priority of this item is less than priority of the next item
                if(thisItem.getPriority() > nextItem.getPriority()){

                    // then swap this item with the next item
                    Object placeholder = cartItems[j]; // make copy of thisItem
                    cartItems[j] = cartItems[j+1]; // move next item to this items position
                    cartItems[j+1] = placeholder; // move this item to next items position
                }
            }
        }
        return cartItems;
    }



    /**
     * This method pretty prints the game instructions to the console as a reference to the user.
     */
    public void showMenuOptions(){
        Console.println("OPTIONS:");
        Console.println("'names'   | displays names of all items in catalog ");
        Console.println("'prices'  | displays prices of all items in catalog");
        Console.println("'stock'   | displays availability of all items in catalog");
        Console.println("'catalog' | displays both name and prices of all items in catalog");
        Console.println("'cart'    | displays items in shopping cart");
        Console.println("'wallet'  | displays budget and current running balance");
        Console.println("'add'     | invokes function to add item to cart");
        Console.println("'remove'  | invokes function to remove item from cart");
        Console.println("'checkout'| invokes function to purchase items in shopping cart");
        Console.println("'!'       | quit program");
    }



    /**
     * This method invokes the checkout process, as follows:
     * 1. In order of highest to least priority value of the CartItems currently in the ShoppingCart,
     *    deduct the cost of the CartItem from the remaining budget, then add the CartItem to a new Array called 'purchasedItems'
     * 2. Continually purchase items until a condition arises such that the cost of the subsequent CartItem is greater than the remaining budget
     * 3. If the aforementioned condition is triggered, do not add the subsequent CartItem to the array of purchased items, and break the loop
     * 4. Print all purchased items and the remaining budget
     */
    public void checkout(){
        Console.debug("Entering Shopper.checkout() method...");
        CartItem[] purchasedItems = cart.checkout( ShoppingGame.store );  // checkout returns CartItem array

        for (int i = 0; i < purchasedItems.length; i++) {
            CartItem purchasedItem = purchasedItems[i];

            /*
            * If we cant buy all items in cart, then purchasedItems will contain null
            * elements. This is because we set purchasedItems equal to the size of the cart.
            * In other words, because we didn't purchase everything in the cart,
            * the cart will contain null elements. The if statement ensures we don't attempt to access
            * null elements.
            */
            if( purchasedItem == null){
                Console.error("No more items can be purchased.");
                break;
            }
            Console.println("Purchased item: " + purchasedItem.getName());
        }
        Console.println("Remaining budget: $" + df.format(budget));

    }



    /**
     * This method pretty prints the remaining budget as calculated by the current budget minus the sum of cost of CartItems currently in the ShoppingCart
     */
    public void getRunningBalance(){
        // used to format cost values down to the penny
        DecimalFormat df = new DecimalFormat("#0.00");

        Console.println("IN WALLET: $" + df.format(budget) );

        Double runningBalance = cart.currentCostInCart();
        Console.println("RUNNING BALANCE (WALLET - CART EXPENSES): " + "$" + df.format(runningBalance));

    }



    /**
     * This method prompts the user to decide whether they want to add items to the cart based on parameters read in from a file, or by manually entering the parameters into the console
     */
    public void addDecisionTree(){

        Scanner sc = new Scanner(System.in); // Initialize new scanner to collect user input

        Console.print("Auto-add shopping list from file? [yes/no]: ");  // Prompt user for option to import shopping list via CSV
        String answer = sc.nextLine();

        if(answer.contains("N") | answer.contains("n")){
            this.addToCartFromInput(ShoppingGame.store);
            return;
        }

        // If input contains Y, proceed.
        if(answer.contains("Y") | answer.contains("y")){
            this.addToCartFromFile(ShoppingGame.store);
        }

        else{
            Console.error("Invalid input. Enter 'Y' or 'N' only.");
        }
    }



    /**
     * This is a mutator method that reads in a shopping list from a file and feeds name, priority, and quantity parameters into the addToCart() method.
     * Note that the actual ShoppingCart manipulation is done through the addToCart() method. This method simply parses the data stream from file into digestible parameter calls.
     * @param store This is the store from which the addToCart() method will attempt to take CartItems
     */
    private void addToCartFromFile(Catalog store){

        Console.println("Enter system path to file. Leave blank for default.");
        Console.println("DEFAULT: " + GroceryStoreConstants.SHOPPINGLIST_FILEPATH);
        Console.print("Enter: ");
        Scanner sc = new Scanner(System.in);
        String filename = sc.nextLine();
        Scanner inputStream = null;

        if(!filename.isEmpty()){
            try{
                inputStream = new Scanner(new File(filename)); // Open and read the file
                String line = inputStream.nextLine(); // Skip the header line by reading and ignoring it
            }
            catch(FileNotFoundException e){
                e.printStackTrace();
            }
            while(inputStream.hasNextLine()){
                String eachLine = inputStream.nextLine();  // This refers literally to each line in the file
                String[] eachItem = eachLine.split(",");  // For every line, build an array containing the name, qty, and priority listed on that line

                String nameInput = eachItem[0];
                String desiredQty = eachItem[1];
                String desiredPriority = eachItem[2];

                Console.debug("nameInput=" + nameInput);
                Console.debug("desiredQty=" + desiredQty);
                Console.debug("desiredPriority=" + desiredPriority);

                this.addToCart(nameInput, desiredQty, desiredPriority, store);
            }
        }
        else{
            try{
                inputStream = new Scanner(new File(GroceryStoreConstants.SHOPPINGLIST_FILEPATH));
                String header = inputStream.nextLine(); // Skip the header line by reading and ignoring it
            }
            catch(FileNotFoundException e){
                e.printStackTrace();
            }
            while(inputStream.hasNextLine()){
                String eachLine = inputStream.nextLine();  // This refers literally to each line in the file
                String[] eachItem = eachLine.split(",");  // For every line, build an array containing the name, qty, and priority listed on that line

                String nameInput = eachItem[0];
                String desiredQty = eachItem[1];
                String desiredPriority = eachItem[2];

                Console.debug("nameInput=" + nameInput);
                Console.debug("desiredQty=" + desiredQty);
                Console.debug("desiredPriority=" + desiredPriority);

                this.addToCart(nameInput, desiredQty, desiredPriority, store);
            }
        }
    }



    /**
     * This method attempts to add the specified CatalogItem from Catalog to ShoppingCart
     * @param store, The store indicates from which Catalog the CatalogItem should be referenced
     */
    public void addToCartFromInput(Catalog store){
        /**
         * This method has 5 steps:
         * 1. Prompts user for an item name (String) and item priority (Integer)
         * 2. Validates that item exists and is available for purchase.
         * 3. Validates that priority is valid.
         * 3. If item matches an existing CatalogItem in store.catalog (List of CatalogItems),
         *    then instantiate a 'CartItem' object based on the matching CatalogItem and priority
         * 4. Remove the CatalogItem from store.catalog
         * 5. Add the 'CartItem' object to the users shopping cart.
         */

        // init scanner
        Scanner sc = new Scanner(System.in);

        // display what is available
        store.whatsInStock();

        // INPUT NAME
        Console.print("\nEnter add NAME: ");  // prompt user to enter item name
        String nameInput = sc.nextLine();  // String will be validated via ItemHandler


        // INPUT QUANTITY
        Console.print("\nEnter add QUANTITY: ");  // prompt user to enter desired quantity of item
        String desiredQty = sc.nextLine();  // String will be validated via ItemHandler


        // INPUT PRIORITY
        Console.print("\nHow important is this item? Items CAN have the same priority." +
                "\nEnter add PRIORITY [1-9]: ");  // prompt user for desired priority
        String desiredPriority = sc.nextLine();  // String will be validated via ItemHandler


        // ATTEMPT TO CREATE ITEM
        this.addToCart(nameInput, desiredQty, desiredPriority, store);
    }



    /**
     * This is a mutator method that attempts to add CartItems to the ShoppingCart.
     * @param nameInput This is the name of the specified CartItem that should be attempted to be added to the ShoppingCart
     * @param desiredQty This is the number of specified CartItems that should be attempted to be added to the ShoppingCart
     * @param desiredPriority This is desired checkout priority that should be assigned to the specified CartItem(s)
     * @param store This is the Catalog from which the CartItems should be taken
     */
    public void addToCart(String nameInput, String desiredQty, String desiredPriority, Catalog store){

        Console.debug("Entering ItemHandler.validateAdd method...");
        if( ItemHandler.validateAdd(nameInput, desiredQty, desiredPriority, cart) ){

            // convert attributes to valid parameter types for CartItem constructor
            Integer priority = Integer.parseInt(desiredPriority);
            Integer qty = Integer.parseInt(desiredQty);
            Double cost = ShoppingGame.store.getItem(nameInput).getCost();  // get cost

            CartItem newItem = new CartItem(nameInput, priority, qty, cost);

            Console.debug("Entered Item Name=" + newItem.getName());
            Console.debug("Entered Item Qty=" + newItem.getQty());
            Console.debug("Entered Item Priority=" + newItem.getPriority());

            // add item(s) to cart
            Console.debug("Entering ShoppingCart.add method...");
            cart.add(newItem, qty, store);
            Console.debug("Add to cart succeeded.");

            // remove item from catalog
            store.removeFromStock(newItem, qty);

            // Display updated cart
            this.showCart();
        }
    }



    /**
     * This method attempts to remove the specified CartItem from ShoppingCart
     * @param cart, The cart represents the ShoppingCart object from which the CartItem should be deducted
     * @param store, The store represents the Catalog to which the CatalogItem contained within the specific CartItem should be returned
     */
    public void removeFromCart(ShoppingCart cart, Catalog store){
        Scanner sc = new Scanner(System.in);
        Console.println("Enter remove NAME: ");
        String nameInput = sc.nextLine();

        Console.println("Enter remove QTY: ");
        String qtyInput = sc.nextLine();  // take in a String instead of Integer to avoid console "fat finger" exceptions

        Console.debug("Entering ItemHandler.validateRemove method...");
        if( ItemHandler.validateRemove(nameInput, qtyInput, cart) ){

            CartItem itemToRemove = cart.getItem(nameInput); // Find CartItem whose name attribute matches the name input

            Console.debug("Entering ShoppingCart.removeFromCart method...");

            cart.removeFromCart(itemToRemove, Integer.valueOf(qtyInput), ShoppingGame.store);  // Remove CartItem(s) from the cart

            Console.debug("Shopper.removeFromCart(): cart.length()=" + cart.length());

            // add item back to catalog stock
            store.returnToStock(itemToRemove, Integer.valueOf(qtyInput), cart);

            // Display updated cart
            this.showCart();
        }

    }




}

