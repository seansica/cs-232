package GroceryStore;


/**
 * Purpose of abstract class is to provide access to static method, validate()
 */
abstract class ItemHandler {


    /**
     * This is a mutator method that returns an Item. It provides a way of safely instantiating new Item objects (either CartItems or
     * CatalogItems) after validating that the input parameters do not break any rules.
     *
     * @param name     This is the desired name of the Item. This method uses this parameter to
     *                 validate that the item is in stock.
     * @param qty      This is the desired quantity of the Item. This method validates whether the desired
     *                 quantity exceeds the reference quantity, i.e. Is the user attempting to purchase more Items
     *                 than are available in the catalog?
     * @param priority This is the desired shopping list priority of the Item. This method validates whether
     *                 the input priority is an integer between 0 and 9.
     * @param cart     This is the ShoppingCart to or from which the Item should either be added or removed, depending
     *                 on the application.
     * @return If no issues exist with any of the parameters, then the method will return a new Item, which can be casted
     * to either a CartItem or CatalogItem.
     */
    public static boolean validateAdd(String name, String qty, String priority, ShoppingCart cart) {

        // validation tests
        try {

            // Ensure specified quantity is a valid integer
            if (!isInteger(qty)){
                throw new InvalidItemPriorityException("Quantity must be an integer between 1-9.");
            }

            // Ensure specified priority is a valid integer
            if (!isInteger(priority)){
                throw new InvalidItemPriorityException("Priority must be an integer between 1-9.");
            }


            // Ensure Catalog contains at least as much item stock as the specified priority
            if (Integer.parseInt(qty) > ShoppingGame.store.getItem(name).getQty()) {
                throw new UnavailableItemException("Not enough of that item in stock!");
            }

            // Given any item, ensure there is enough stock to satisfy the specified desired quantity
            if (!ShoppingGame.store.getItem(name).isInStock()) {
                throw new UnavailableItemException("That item is out of stock!");
            }

            // Ensure that specified item is contained in the Catalog
            if (!ShoppingGame.store.contains(name)){
                throw new UnavailableItemException("Specified item does not exist.");
            }

            // If specified CartItem already exists in ShoppingCart, ensure that specified priority matches priority value of existing CartItem
            // i.e. Don't allow duplicate items with different priorities
            for (Object o : cart.getCartItems()) {
                CartItem item = (CartItem) o;  // cast each indexed Object to CartItem

                // Condition match: names are same but priorities are different
                if (!(item.getPriority().equals(Integer.parseInt(priority))) && item.getName().equals(name)) {
                    throw new InvalidItemPriorityException("Specified priority must match priority of matching cart item.");
                }

            }
        } catch (UnavailableItemException | InvalidItemPriorityException e) {
            e.printStackTrace();
            Console.error("ItemHandler failed.");
            return false;
        }
        Console.debug("ItemHandler succeeded.");
        return true;
    }



    /**
     * This method contains the logic used to determine if the specified name and/or quantity will cause any nonsensical,
     * undesirable, or otherwise erroneous conditions as a result of passing them into the removeFromCart() method. This method is a critical safety net.
     * @param name This is the specified item name that should be scrutinized
     * @param qty This is the specified item quantity that should be scrutinized
     * @param cart This is the cart from which the specified item should be scrutinized
     */
    public static boolean validateRemove(String name, String qty, ShoppingCart cart) {

        try{
            // ensure qty parameter contains a valid integer
            if (!isInteger(qty)){
                throw new InvalidItemPriorityException("Quantity must be an integer between 1-9.");
            }
        }
        catch (InvalidItemPriorityException e) {
            e.printStackTrace();
        }

        try{
            Console.debug("There are " + cart.length() + " items in the cart.");
            for(Object o: cart.getCartItems()) {

                // Cast Object to CartItem
                CartItem item = (CartItem)o;

                // determine whether the cart contains the item
                if( item.getName().toLowerCase().equals(name.toLowerCase())){

                    // ensure we are not attempting to remove more items than are available
                    if(item.getQty() < Integer.parseInt(qty)){
                        Console.error("Entered quantity exceeds available quantity.");
                        return false;
                    }
                    Console.debug("ItemHandler.validateRemove succeeded.");
                    return true;
                }
            }
        }
        catch(UnsupportedOperationException e){
            e.printStackTrace();
        }
        Console.error("ItemHandler.validateRemove failed.");
        return false;
    }



    /**
     * This method determines whether the specified String parameter contains and ONLY contains a digit
     * @param s This is the specified String which should be scrutinized
     * @return Returns true if parameter is a digit, and false otherwise
     */
    private static boolean isInteger(String s){

        // determine whether string is empty
        if(s.isEmpty()){
            return false;
        }

        // iterate over each character in string array
        for (int i = 0; i < s.length(); i++) {

            // if any character does not match an integer in the range 0-9
            if( !String.valueOf( s.toCharArray()[i] ).matches("[0-9]") ){
                return false;
            }
        }
        return true;
    }


}

