package GroceryStore;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class ShoppingCart {

    //attributes
    private PriorityQueue<CartItem> cart;  // list of items in cart, sorted by MyItem.priority


    // constructor
    public ShoppingCart(){
        cart = new PriorityQueue<>();
    }


    // METHODS

    public boolean contains( String itemName ){

        for (int i = 0; i < this.cart.size(); i++) {
            CartItem evalItem = cart.peek();
            if( evalItem.getName().toLowerCase() == itemName.toLowerCase()){
                return true;
            }
        }
        return false;
    }



    public void add(CartItem newItem, Integer qty, Catalog store ){

        // ensure cart does not exceed max size
        try{
            if( this.cart.size() == GroceryStoreConstants.MAX_CART_SIZE ){
                throw new UnavailableItemException( "ERROR! Max cart size reached." );
            }
            // determine whether store contains the desired item
            if( !store.contains(newItem.getName()) ){
                throw new UnavailableItemException( "ERROR! That item is not available." );
            }

            // match found: ensure cart does not already contain desired item
            if( !this.cart.contains( newItem ) ){

                // iterate equal to the number of desired items
                for (int i = 0; i < qty; i++) {

                    // add item(s) to cart
                    this.cart.add( newItem );
                }
            }
        }
        catch(UnsupportedOperationException | UnavailableItemException e){
            System.out.println(e.getMessage());
        }
    }



    public void removeFromCart( CartItem item, Integer qty, Catalog store){

        // convert priority queue to object array
        Object[] tempArray = cart.toArray();

        // create counter
        int removeQtyCounter = qty;
        Console.debug("removeCounter=" + removeQtyCounter);

        // iterate over cart array
        for (int i = 0; i < tempArray.length; i++) {

            // cast each item in array to CartItem class
            CartItem currentItem = (CartItem)tempArray[i];

            // stop if remove amount is zero (or reaches zero)
            if(removeQtyCounter <= 0) {
                Console.debug("removeCounter reached " + removeQtyCounter + ". Break method.");
                break;
            }

            if(item.getQty() == 0){
                Console.debug("Item '" + item.getName() + "' quantity is " + item.getQty() + ". Can't remove any more items.");
                break;
            }

            // match found
            if(currentItem.equals(item)){
                Console.debug("Found matching item!");
                cart.remove(item);  // remove item
                Console.debug(item.getName() + " removed from cart.");
                removeQtyCounter--;  // decrement remove counter; will stop at zero or when iteration ends
                Console.debug("removeCounter decremented to " + removeQtyCounter);
            }
        }
    }



    public CartItem[] checkout( Catalog store ){
        /*
        * Method Workflow:
        * 1. Start with a budget and two containers: cart and purchasedItems
        * 2. If highest priority item in cart does not exceed budget,
        *    then subtract cost of item from budget and add item to purchasedItems container
        * 3. Repeat Step 2 until remaining budget <= 0
        * 4. Return purchasedItems container
         */

        CartItem[] purchasedItems = new CartItem[cart.size()];
        int baggedItem = 0;

        while( !cart.isEmpty() ){

            // TEST
            Console.debug("ShoppingCart size=" + cart.size());
            Console.debug("Shopper budget=" + Shopper.getBudget());
            Console.debug("cart.peek()=" + cart.peek());
            Console.debug("cart.peek().getCost()=" + cart.peek().getCost());

            // break if budget less than or equal to zero
            if( Shopper.getBudget() < cart.peek().getCost() ){
                Console.debug("BUDGET=" + Shopper.getBudget());
                return purchasedItems;
            }

            assert cart.peek() != null;
            Shopper.setBudget( cart.peek().getCost() );  // subtract cost of item
            CartItem currentItem = cart.poll();  // pop off highest priority item from pQueue


            purchasedItems[baggedItem] = currentItem;  // add purchased item to array
            baggedItem++;  // increment counter
        }
        return purchasedItems;
    }



    public Integer length(){
        return cart.size();
    }



    public Double currentCostInCart(){

        Double remainingBudget = GroceryStoreConstants.MAX_BUDGET;

        for (int i = 0; i < cart.size(); i++) {
            remainingBudget -= cart.peek().getCost();
        }
        return remainingBudget;
    }



    public Object[] getCartItems(){
        return cart.toArray();
    }



    public void reset(){

        //remove all items from cart
        for (int i = 0; i < cart.size(); i++) {
            Console.debug(cart.peek().getName() + " removed from cart.");
            cart.remove();
        }
        Console.debug("Cart reset to default=SUCCESS");
    }



    public CartItem getItem(String itemName){

        CartItem returnItem = null;  // Initialize CartItem placeholder

        Object[] tempArray = this.getCartItems();  // Convert cart to iterable Array

        for (int i = 0; i < tempArray.length; i++) {

            // Cast each Object to CartItem
            CartItem tempItem = (CartItem)tempArray[i];

            // If current item in iteration has name attribute as parameter String
            if (tempItem.getName().toLowerCase().equals(itemName.toLowerCase())) {
                returnItem = tempItem;
            }
        }
        return returnItem;
    }
}
