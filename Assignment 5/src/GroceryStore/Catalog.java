package GroceryStore;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class Catalog {

    // private attributes - items
    private CatalogItem apple = new CatalogItem( "apple");
    private CatalogItem orange = new CatalogItem("orange");
    private CatalogItem banana = new CatalogItem("banana");
    private CatalogItem kiwi = new CatalogItem("kiwi");
    private CatalogItem avocado = new CatalogItem("avocado");
    private CatalogItem mango = new CatalogItem("mango");
    private CatalogItem blueberry = new CatalogItem("blueberry");
    private CatalogItem watermelon = new CatalogItem("watermelon");
    private CatalogItem melon = new CatalogItem("melon");
    private CatalogItem strawberry = new CatalogItem("strawberry");
    private CatalogItem blackberry = new CatalogItem("blackberry");


    /**
     * @attribute catalog, This is the Catalog. It is a simple ArrayList that contains objects instantiated
     *            from the CatalogItems class.
     * @attribute df, This is a DecimalFormatter that handles pretty printing monetary
     *            values to the nearest penny.
     */
    private ArrayList<CatalogItem> newCatalog = new ArrayList<>();
    DecimalFormat df = new DecimalFormat("#0.00");



    /**
     * This is the default constructor. It adds all of the CatalogItems that are instantiated by the class by default
     * and puts them into an ArrayList.
     */
    public Catalog(){

        // populate the catalog
        this.newCatalog.add(this.apple);
        this.newCatalog.add(this.orange);
        this.newCatalog.add(this.banana);
        this.newCatalog.add(this.kiwi);
        this.newCatalog.add(this.avocado);
        this.newCatalog.add(this.mango);
        this.newCatalog.add(this.blueberry);
        this.newCatalog.add(this.watermelon);
        this.newCatalog.add(this.melon);
        this.newCatalog.add(this.strawberry);
        this.newCatalog.add(this.blackberry);

        // set default attribute values (cost, qty, and status)
        for (CatalogItem item: newCatalog) {
            item.setCost(GroceryStoreConstants.DEFAULT_CATALOG_COST);  // override default cost
            item.setCartStatus(false);  // override default status to 'not purchased'
            item.setQty(GroceryStoreConstants.DEFAULT_CATALOG_QTY);  // override default qty to 10
        }
    }



    /**
     * This is an accessor method that prints all itemName String attributes of all Item objects in the Catalog.
     */
    public void getNames(){
        for(CatalogItem item: newCatalog){
            String currentItem = item.getName();
            System.out.println( "ITEM NAME: " + currentItem );
        }
    }



    /**
     * This is an accessor method that prints all cost Double attributes of all Item objects in the Catalog.
     */
    public void getCosts(){
        for(CatalogItem item: newCatalog){

            String currentItemCost = df.format(item.getCost());
            String currentItemName = item.getName();
            System.out.println( "COST OF " + currentItemName + ": $" + currentItemCost );
        }
    }



    /**
     * This is an accessor method that prints whether each Item in the Catalog is in or out of stock,
     * as determined by the qty attribute.
     */
    public void whatsInOutOfStock() {
        for (CatalogItem item: newCatalog) {
            String currentItemName = item.getName();
            if(item.getQty() > 0){
                System.out.println( "Item " + currentItemName + " is in stock.");
            }
            if(item.getQty() == 0){
                System.out.println("Item " + currentItemName + " is out of stock.");
            }
        }
    }



    /**
     * This is an accessor method that only prints the Item object that are in stock, as determined
     * by the qty attribute as being greater than zero. This is done for each item in the Catalog.
     */
    public void whatsInStock() {
        Console.debug("Entering Catalog.whatsInStock method...");

        for (CatalogItem item: newCatalog) {
            String currentItemName = item.getName();
            if(item.getQty() > 0){
                Console.println("Item " + currentItemName + " is in stock.");
            }
        }
    }



    /**
     * This is an accessor method that pretty prints all Items in the Catalog along with their attribute statuses.
     */
    public void getCatalog(){
        Console.debug("Entering Catalog.getCatalog method...");

        for (CatalogItem item: newCatalog) {
            String currentItemCost = df.format(item.getCost());
            String currentItemName = item.getName();
            Integer qtyInStock = item.getQty();
            boolean stockStatus = item.isInStock();
            Console.println("Item " + currentItemName + ", Cost $" + currentItemCost +
                    ", QTY=" + qtyInStock + ", Available=" + stockStatus);
        }
    }



    /**
     * This is an accessor method that determines whether the Catalog contains the specified Item object.
     * @param compareToItem This is the Item object that will be matched against in the Catalog search
     * @return true if Catalog contains the specified Item object or,
     *         false if Catalog does not contain the specified Item object
     */
    public boolean contains( CatalogItem compareToItem ){
        /*
        * Purpose: Determines if the store contains the CatalogItem which is passed in the method parameters
         */
        for (CatalogItem item: newCatalog) {

            // item in stock: return true
            if ( item.getName().toLowerCase().equals( compareToItem.getName().toLowerCase() )){
                Console.debug("Catalog.contains(" + compareToItem.getName() +")=" + compareToItem.isInStock());
                return true;
            }
        }
        // no match found: return false
        Console.error("Catalog.contains(" + compareToItem.getName() +")=" + compareToItem.isInStock());
        return false;
    }



    /**
     * This is an accessor method that determines whether the Catalog contains the specified Item object.
     * @param compareToItem This is the Item objects itemName String attribute that will be matched against in the Catalog search
     * @return true if Catalog contains the specified Item object or,
     *         false if Catalog does not contain the specified Item object
     */
    public boolean contains( String compareToItem ){
        Console.debug("Entering Catalog.contains(String) method...");

        for (CatalogItem item: newCatalog) {

            if( item.getName().toLowerCase().equals( compareToItem.toLowerCase() )){
                Console.debug("Catalog.contains(" + compareToItem +")=true");
                return true;
            }
        }
        Console.error("Catalog.contains(" + compareToItem +")=false");
        return false;
    }



    /**
     * This is a mutator method that removes the specified CatalogItem object from the Catalog. Removal from
     * stock is qualified as any CatalogItem object whose inCart boolean attribute is set to true.
     * @param itemName This is the specified CatalogItem object that should be removed from the Catalog
     */
    public void removeFromStock(Item itemName, Integer qty){

        Console.debug("Entered Catalog.removeFromStock method...");

        for (CatalogItem item: newCatalog) {

            // iterate over catalog to find specified item based on name attribute
            if( item.getName().toLowerCase().equals(itemName.getName().toLowerCase()) ){

                Console.debug("Found matching item (" + item.getName() + ") to remove from Catalog!");

                // check if desired quantity exceeds available amount in stock
                if(qty > item.getQty()){
                    Console.debug("Specified purchase QTY=" + qty);
                    Console.debug("Available stock QTY=" + item.getQty());
                    Console.debug("Not enough " + item.getName() + "s in Catalog!");
                    break;
                }

                // deduct desired qty from stock
                Integer newQty = item.getQty() - qty;
                Console.debug("PRE Stock Status=" + item.isInStock());
                item.setQty(newQty);
                Console.debug(qty + " " + item.getName() + "s removed from Catalog!");

                // update availability of item
                item.setStockStatus();
                Console.debug("POST Stock Status=" + item.isInStock());
            }
        }
    }



    /**
     * This is a mutator method that adds the specified CatalogItem object back to the Catalog.
     * @param returnItem This is a CartItem that contains the specified
     */
    public void returnToStock(CartItem returnItem, Integer qty, ShoppingCart cart){

        Console.debug("Entering Catalog.returnToStock method...");

        // add item back to catalog stock
        for (CatalogItem item: newCatalog) {

            // Ensure Catalog contains the specified item
            if( item.getName().toLowerCase().equals( returnItem.getName().toLowerCase() )){

                Console.debug("Catalog.returnToStock(): Found specified CartItem (" + returnItem.getName() + ") in Catalog. Proceeding.");

                /*
                The purpose of the following block is to handle condition where user enters a
                quantity (as part of the item removal process) that is greater than the
                quantity of the corresponding CartItem currently in the cart
                i.e. Cart has 4 apples; User tries to remove 10 apples; override entered
                     quantity (10) to 4.
                 */

                // Initialize Cart Array to find specified CartItem
                Object[] tempArray = cart.getCartItems();
                Console.debug("tempArray.length=" + tempArray.length);

                // Iterate over Cart Array
                for (int i = 0; i < tempArray.length; i++) {

                    // Initialize CartItem container for readability
                    CartItem cartItem = (CartItem)tempArray[i];
                    Console.debug("cartItem=" + cartItem.getName());

                    Console.debug("Catalog.returnToStock(): cartItem.getName()=" + cartItem.getName().toLowerCase());
                    Console.debug("Catalog.returnToStock(): returnItem.getName()=" + returnItem.getName().toLowerCase());

                    // Iterate over Cart Array until specified item located
                    if(cartItem.getName().toLowerCase().equals(returnItem.getName().toLowerCase())){

                        Console.debug("Catalog.returnToStock(): Found specified CartItem (" + returnItem.getName() + ") in Cart. Proceeding.");

                        // If user-specified quantity is greater than QTY added to cart, use cart QTY instead
                        if(cartItem.getQty() < qty){

                            Console.debug("Item in cart has less quantity (" + cartItem.getQty() + ")than specified quantity (" + qty + ").");

                            qty = cartItem.getQty();

                            Console.debug("QTY OVERRIDDEN TO " + qty);
                        }
                    }
                }
                // add specified qty of item back to stock
                Console.debug("Found matching item!");
                Console.debug("PRE QTY=" + item.getQty());
                Console.debug("PRE Stock Status=" + item.getQty());

                Integer newQty = item.getQty() + qty;
                item.setQty(newQty);

                Console.debug("POST QTY=" + item.getQty());

                item.setStockStatus();

                Console.debug("POST STOCK STATUS=" + item.isInStock());
            }
        }
    }


    /**
     * This is an accessor method that returns the CatalogItem based on the specified String
     * @param itemName This is a String that should match the itemName attribute of the intended Item object
     * @return This is the CatalogItem object that contains the specified itemName attribute
     */
    public CatalogItem getItem( String itemName ){

        CatalogItem matchedItem = new CatalogItem();

        try{
            // iterate over items in catalog
            for (CatalogItem item: newCatalog) {

                // find item whose name matches method parameter 'itemName'
                if( item.getName().equals(itemName) ){

                    // match found!
                    matchedItem = item;  // return item;
                }
            }

            // iteration completed & no match found
            if(!matchedItem.getName().equals(itemName)){
                throw new UnavailableItemException("Item (" +
                        itemName + ") not found.");
            }
        }
        catch(UnavailableItemException e){
            e.printStackTrace();
        }

        return matchedItem;
    }



    /**
     * This is a mutator method that resets the cost and cart status of all Items contained in Catalog
     * to their default values.
     */
    public void reset() {

        // reset catalog values to default
        for (CatalogItem item: newCatalog) {
            item.setCost(GroceryStoreConstants.DEFAULT_CATALOG_COST);  // override default cost
            item.setQty(GroceryStoreConstants.DEFAULT_CATALOG_QTY);  // reset QTY to default
            item.setCartStatus(false);  // override default status to 'not purchased'
        }
        Console.debug("Catalog reset to default: SUCCESS");
    }
}
