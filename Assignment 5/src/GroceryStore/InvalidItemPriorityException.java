package GroceryStore;

/**
 * The purpose of this Exception class is to handle invalid conditions under which the desired priority of a CartItem
 * is nonsensical. For example, it does not make sense to specify a priority of 8 to an item when only 7 items
 * exist in the cart.
 */
public class InvalidItemPriorityException extends Exception{

    /**
     * This is the constructor; it simply passes a parameter String to the Exception class constructor.
     * @param msg This parameter refers to a user-defined String. It is suggested that the message
     *            elaborate why specified priority is invalid or not allowed.
     */
    public InvalidItemPriorityException(String msg){
        Console.error(msg);
        super.printStackTrace();
    }

}
