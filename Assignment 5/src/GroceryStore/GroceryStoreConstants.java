package GroceryStore;

public class GroceryStoreConstants {

    public static final Double MAX_BUDGET = 59.00;

    public static final Integer MAX_CART_SIZE = 20;

    public static final Integer DEFAULT_CATALOG_QTY = 10;

    public static final Double DEFAULT_CATALOG_COST = 10.00;

    public static final String SHOPPINGLIST_FILEPATH = "/Users/seansica/Documents/Boston University/CS-232 Programming with Java/Assignment 5/src/GroceryStore/shoppinglist.txt";

    public static final boolean SYSLOG_DEBUG = false;

    public static final boolean SYSLOG_ERROR = true;

}
