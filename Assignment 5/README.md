# Shopping Game

This program builds on the additions made to Shopping Simulator in Assignment 4.

# New Features
- Now supports quantities! You can add/remove multiples of same item!
- Now supports importing shopping list from CSV file!
- Now supports toggling of DEBUG and ERROR messages!
- Now supports writing DEBUG and ERROR messages to log file...with timestamps!
- Default Catalog attributes (default qty & cost) can now be set as a global variable in _GroceryStoreConstants_.

# New Files
All Class files are located in **src/GroceryStore**.
- **ItemHandler**
	- The logic which handles adding and removing items to the ShoppingCart and Catalog have been split into its own abstract class.
- **Console**
	- This is an abstract class which centrally handles all console output for the program.
	- _Console.debug(String)_ and _Console.error(String)_ can be toggled on/off in the program. They also print text in Blue and Red, respectively.
- **InvalidItemPriorityException**
    - Custom Exception class for implicit error explanation. Nothing special.
- **UnavailableItemException**
    - Same as above. Just another sub-Exception class that throws error messages.

## Bug Fixes
I believe all possible Exceptions have been caught and handled; however I chose not to recover intelligently from certain Exceptions. For example, if you attempt to remove 10 Items from the Cart wherein only 5 of that Item already exists in the Cart, the program will throw an Error and ignore the remove request, rather than intelligently recover (i.e. re-prompt the user for a valid qty, or assume the user wants to remove all of the Item and proceed anyways.)

Please feel free to PM me if you find any bugs.
 
## Class Relationships UML
![UML](Assignment 5 UML Diagram.png)