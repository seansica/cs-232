# Shopping Game

This program simulates a shopping experience by allowing the user to interact with a shopping cart and a store catalog via the console. The user is restricted by a budget and must purchase items in order priority values.

The shopping cart and store catalog are managed in separate data structures. The shopping cart is a PriorityQueue under the hood whereas the store catalog is an array of items. The elements of both the pQueue and array are different class types as well; the store contains CatalogItems and the cart contains CartItems.

# Features
- names: Display all items in store catalog
- prices: Display all prices in store catalog
- stock: Displays availability of all items in catalog
- Catalog: Displays both name and prices of all items in catalog
- cart: Displays items in shopping cart
- wallet: Displays budget and current running balance
- add: Invokes function to add item to cart
- remove: Invokes function to remove item from cart
- checkout: Invokes function to purchase items in shopping cart

# Files
All Class files are located in **src/GroceryStore**.
- **ShoppingGame.java**
	- This is where the Main method executes.
- **Shopper.java**
	- This Class is where all of the methods, variable manipulation, and user interaction occurs. The user is contained within an infinite while loop in which they interact with the console.
- **ShoppingCart.java**
    - This Class contains all CartItems as defined by the user via the add() method.
- **CartItem.java**
    - CartItems contain CatalogItems and priority values as defined by the user.
- **Catalog.java**
    - The Catalog Class contains CatalogItems and various methods to interact with those items.
- **CatalogItem.java**
    - CatalogItems each contain a item name, cost, and boolean that determines whether the item has been added to ShoppingCart yet.

## Limitations
At this time, there are no known bugs that will cause the program to crash. In addition, all known Class & Primitive limitations have been considered and caught within the code.

## Class Relationships UML
![UML](Assignment 3 UML.png)