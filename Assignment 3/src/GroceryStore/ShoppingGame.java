package GroceryStore;

public class ShoppingGame {

    //attributes
    public static Catalog store = new Catalog();
    private static Shopper shopper = new Shopper();

    public static void main( String[] args ){

        shopper.startGame();  // start game
    }
}
