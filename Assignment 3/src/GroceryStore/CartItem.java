package GroceryStore;


class CartItem implements Comparable {
    /*
     * MyItem subclass inherits from Comparable interface
     * Reason: need way of sorting cart container by the elements attributes.
     * In this case, we will be sorting the cart container by MyItem.priority
     */


    // attributes
    private CatalogItem item;
    private String itemName;
    private Integer priority;


    // constructor
    CartItem(String itemName, Integer itemPriority, Catalog store) {

        // determine whether the store contains the desired item
        if (!store.contains(itemName)) {
            throw new UnsupportedOperationException("ERROR! Store does not contain " + itemName);
        }

        // determine whether matching item is in stock
        if (!store.getItem(itemName).isInCart()) {
            throw new UnsupportedOperationException("ERROR! Item not available.");
        }

        // determine whether valid priority value entered
        if (!itemPriority.toString().matches("[0-7]")) {
            throw new UnsupportedOperationException("ERROR! Invalid priority number entered.");
        }

        this.itemName = itemName;

        // store does contain item: clone the matching catalog item
        this.item = store.getItem(itemName);

        // valid number entered: assign priority
        this.priority = itemPriority;

    }


    // accessor methods
    public Integer getPriority(){
        return this.priority;
    }


    public CatalogItem getItem(){
        return this.item;
    }


    public Double getCost(){
        return this.item.getCost();
    }


    // mutator methods
    public void setPriority( Integer newPriority ){
        this.priority = newPriority;
    }


    public void setItem( CatalogItem newItem ){
        this.item = newItem;
    }


    public boolean equals( CartItem compareItem ){

        String thisItemName = this.item.getName().toLowerCase();
        String thatItemName = compareItem.getItem().getName().toLowerCase();

        if( thisItemName.equals( thatItemName ) ){
            return true;
        }
        return false;
    }


    public boolean equals( CatalogItem compareItem ){

        String thisItemName = this.item.getName().toLowerCase();
        String thatItemName = compareItem.getName().toLowerCase();

        if( thisItemName.equals( thatItemName ) ){
            return true;
        }

        return false;
    }


    @Override
    public int compareTo(Object o) {
        /*
        * Purpose: Override superclasses compareTo() method because we want to always sort
        *          the PriorityQueue by MyItem.priority integer.
         */
        CartItem other = (CartItem)o;
        return Integer.compare( this.priority, other.getPriority() );
    }


    @Override
    public String toString() {
        /*
        * Purpose: Override superclass toString() method to make printing (str)attributes in a meaningful way,
        *          rather than printing the entire MyItem object in memory.
         */
        return "MyItem{" +
                "itemName='" + itemName + '\'' +
                ", priority=" + priority +
                '}';
    }
}
