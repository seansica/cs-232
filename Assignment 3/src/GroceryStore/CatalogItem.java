package GroceryStore;

public class CatalogItem {

    // attributes
    private String name;  // refers to name of each catalog item; default is empty string ""
    private boolean inCart;  // refers to whether item is currently in shopping cart
    private Double cost;  // refers to cost of each catalog item; default is $9.00


    // constructor 1
    public CatalogItem(){
        this.name = "";  // default no name
        this.cost = 0.00;  // default $0.00
        this.inCart = false;  // default not in cart
    }


    // constructor 2
    public CatalogItem( String itemName ){
        this.name = itemName;
        this.cost = 0.00;  // default value is $2.00
        this.inCart = false;  // default not in cart
    }


    // METHODS


    public String getName(){
        return this.name;
    }


    public Double getCost(){
        return this.cost;
    }


    public boolean isInCart(){
        if(this.inCart){
            return false;
        }
        return true;
    }


    public void setName( String name ){
        this.name = name;
    }


    public void setCost( Double cost ) {
        this.cost = cost;
    }


    public void setCartStatus(boolean inCart ){
        this.inCart = inCart;
    }

}
