package GroceryStore;

import java.util.PriorityQueue;

public class ShoppingCart {

    //attributes
    private PriorityQueue<CartItem> cart;  // list of items in cart, sorted by MyItem.priority


    // constructor
    public ShoppingCart(){
        cart = new PriorityQueue<>();
    }


    // METHODS

    public boolean contains( String itemName ){

        for (int i = 0; i < this.cart.size(); i++) {
            CartItem evalItem = cart.peek();
            if( evalItem.getItem().getName().toLowerCase() == itemName.toLowerCase()){
                return true;
            }
        }
        return false;
    }


    public void add(CartItem newItem, Catalog store ){

        // ensure cart does not exceed max size
        try{
            if( this.cart.size() == GroceryStoreConstants.MAX_CART_SIZE ){
                throw new UnsupportedOperationException( "ERROR! Max cart size reached." );
            }
            // determine whether store contains the desired item
            if( !store.contains(newItem.getItem()) ){
                throw new UnsupportedOperationException( "ERROR! That item is not available." );
            }

            // match found: ensure cart does not already contain desired item
            if( !this.cart.contains( newItem ) ){

                // no duplicates found: add item to cart
                this.cart.add( newItem );
            }
        }
        catch(UnsupportedOperationException e){
            System.out.println(e.getMessage());
        }
    }


    public void removeFromCart( CartItem item, Catalog store ){

        // remove item from cart
        cart.remove( item );

        // add item back to catalog stock
        store.returnToStock( item );
    }


    public void removeFromCart( String itemName, Catalog store ){

        // stop if cart contains nothing
        try{
            if( cart.size() == GroceryStoreConstants.MIN_CART_SIZE ){
                throw new UnsupportedOperationException("ERROR! Can't remove any more items.");
            }

            for (int i = 0; i < cart.size(); i++) {

                if( cart.contains(itemName) ){
                    // remove item from cart
                    cart.remove( itemName );

                    // add item back to catalog stock
                    store.returnToStock( itemName );
                }
            }
        }
        catch(UnsupportedOperationException e){
            System.out.println(e.getMessage());
        }
    }


    public CartItem[] checkout( Catalog store ){
        /*
        * Method Workflow:
        * 1. Start with a budget and two containers: cart and purchasedItems
        * 2. If highest priority item in cart does not exceed budget,
        *    then subtract cost of item from budget and add item to purchasedItems container
        * 3. Repeat Step 2 until remaining budget <= 0
        * 4. Return purchasedItems container
         */

        CartItem[] purchasedItems = new CartItem[cart.size()];
        int baggedItem = 0;

        while( !cart.isEmpty() ){

            // break if budget less than or equal to zero
            if( Shopper.getBudget() < cart.peek().getItem().getCost() ){
                System.out.println("FAIL: BUDGET=" + Shopper.getBudget());
                return purchasedItems;
            }

            Shopper.setBudget( cart.peek().getCost() );  // subtract cost of item
            CartItem currentItem = cart.poll();  // pop off highest priority item from pQueue


            purchasedItems[baggedItem] = currentItem;  // add purchased item to array
            baggedItem++;  // increment counter
        }
        return purchasedItems;
    }


    public Integer length(){
        return cart.size();
    }


    public Double currentCostInCart(){

        Double remainingBudget = GroceryStoreConstants.MAX_BUDGET;

        for (int i = 0; i < cart.size(); i++) {
            remainingBudget -= cart.peek().getItem().getCost();
        }
        return remainingBudget;
    }


    public Object[] getCartItems(){
        return cart.toArray();
    }


    public String getItemName( String itemName ){
        for (int i = 0; i < cart.size(); i++) {
            if( cart.contains(itemName) ){
                return cart.peek().getItem().getName();
            }
        }
        throw new UnsupportedOperationException("Item " + itemName + " is not contained in the store.");
    }


    public void reset(){

        //remove all items from cart
        for (int i = 0; i < cart.size(); i++) {
            cart.remove();
        }
        System.out.println("Cart reset to default: SUCCESS");
    }


//    unit test for ensuring PriorityQueue returns elements in correct order
//    public static void main( String[] args ){
//
//        PriorityQueue<MyItem> testQueue = new PriorityQueue<>();
//        testQueue.add( new MyItem("orange",3));
//        testQueue.add( new MyItem("apple",1));
//        testQueue.add( new MyItem("grape",2));
//
//        System.out.println(testQueue.poll());
//        System.out.println(testQueue.poll());
//        System.out.println(testQueue.poll());
//    }


}
