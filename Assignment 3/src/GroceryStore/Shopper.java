package GroceryStore;

import java.text.DecimalFormat;
import java.util.Scanner;


public class Shopper {


    // attributes
    private static Double budget;
    private DecimalFormat df = new DecimalFormat("#0.00");
    private ShoppingCart cart = new ShoppingCart();


    // constructor
    Shopper() {
        budget = 59.00;
        this.startGame();
    }


    // public methods
    void startGame() {
    // this method is where all user input occurs. the user never leaves this method.

        // game start: welcome!
        System.out.println("S H O P P I N G   G A M E");
        // instructions
        this.showMenuOptions();

        // user input
        System.out.print("Enter option: ");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();

        // main menu loop
        while (!input.equals("!")) {

            switch (input) {

                case "names":
                    ShoppingGame.store.getNames();
                    break;

                case "prices":
                    ShoppingGame.store.getCosts();
                    break;

                case "stock":
                    ShoppingGame.store.whatsInOutOfStock();
                    break;

                case "catalog":
                    ShoppingGame.store.getCatalog();
                    break;

                case "add":
                    this.addToCart(ShoppingGame.store);
                    break;

                case "cart":
                    this.showCart();
                    break;

                case "remove":
                    this.removeFromCart(cart, ShoppingGame.store);
                    break;

                case "wallet":
                    this.getRunningBalance();
                    break;

                case "checkout":
                    this.checkout();

                    // game over: reset budget
                    this.resetGame();
                    break;
            }
            this.showMenuOptions();
            System.out.print("Enter option: ");
            input = sc.nextLine();
        }
        System.out.println("GAME OVER");
    }


    private void resetGame( ) {
        this.resetBudget();
        cart.reset();
        ShoppingGame.store.reset();
    }


    private void resetBudget() {
        budget = GroceryStoreConstants.MAX_BUDGET;
        System.out.println("Budget reset to $" + df.format(budget) + ": SUCCESS");
    }


    public static void setBudget(Double itemCost) {
        /*
         * Purpose: subtract cost of item from budget
         */
        budget -= itemCost;
    }


    public static Double getBudget(){
        return budget;
    }


    private void showCart() {
        Object[] cartItems = cart.getCartItems();

        for (int i = 0; i < cartItems.length; i++) {
            CartItem cartItem = (CartItem) cartItems[i];

            String msg = "CartItem{" +
                    "itemName='" + cartItem.getItem().getName() + '\'' +
                    ", priority=" + cartItem.getPriority() +
                    '}';

            System.out.println(msg);
        }
    }

    /*

     */
    private void showMenuOptions(){
        System.out.println("OPTIONS:");
        System.out.println("'names'   | displays names of all items in catalog ");
        System.out.println("'prices'  | displays prices of all items in catalog");
        System.out.println("'stock'   | displays availability of all items in catalog");
        System.out.println("'catalog' | displays both name and prices of all items in catalog");
        System.out.println("'cart'    | displays items in shopping cart");
        System.out.println("'wallet'  | displays budget and current running balance");
        System.out.println("'add'     | invokes function to add item to cart");
        System.out.println("'remove'  | invokes function to remove item from cart");
        System.out.println("'checkout'| invokes function to purchase items in shopping cart");
        System.out.println("'!'       | quit program");
    }


    private void checkout(){
        System.out.println("Checking out...");
        CartItem[] purchasedItems = cart.checkout( ShoppingGame.store );  // checkout returns CartItem array

        for (int i = 0; i < purchasedItems.length; i++) {
            CartItem purchasedItem = purchasedItems[i];

            /*
            * If we cant buy all items in cart, then purchasedItems will contain null
            * elements. This is because we set purchasedItems equal to the size of the cart.
            * In other words, because we didn't purchase everything in the cart,
            * the cart will contain null elements. The if statement ensures we don't attempt to access
            * null elements.
            */
            if( purchasedItem == null){
                System.out.println("ERROR! No more items can be purchased.");
                break;
            }
            System.out.println("Purchased item: " + purchasedItem.getItem().getName());
        }
        System.out.println("Remaining budget: $" + df.format(budget));

    }


    private void getRunningBalance(){
        // used to format cost values down to the penny
        DecimalFormat df = new DecimalFormat("#0.00");

        System.out.println("IN WALLET: $" + df.format(budget) );

        Double runningBalance = cart.currentCostInCart();
        System.out.println("RUNNING BALANCE (WALLET - CART EXPENSES): " + "$" + df.format(runningBalance));

    }


    private void addToCart( Catalog store ){
        /**
         * This method has 5 steps:
         * 1. Prompts user for an item name (String) and item priority (Integer)
         * 2. Validates that item exists and is available for purchase.
         * 3. Validates that priority is valid.
         * 3. If item matches an existing CatalogItem in store.catalog (List of CatalogItems),
         *    then instantiate a 'CartItem' object based on the matching CatalogItem and priority
         * 4. Remove the CatalogItem from store.catalog
         * 5. Add the 'CartItem' object to the users shopping cart.
         */

        // init scanner
        Scanner sc = new Scanner(System.in);

        // display what is available
        store.whatsInStock();

        // prompt user to enter item name
        System.out.print("\nPlease enter an item name: ");
        String nameInput = sc.nextLine();

        System.out.print("\nHow important is this item? Items cannot have the same priority." +
                "\nEnter priority [1-7]: ");
        Integer priorityInput = sc.nextInt();

        try{
            if (!store.contains(nameInput)) {
                throw new UnsupportedOperationException("ERROR! " +  nameInput + " is never contained in catalog.");
            }

            // determine whether matching item is in stock
            if (!store.getItem(nameInput).isInCart()) {
                throw new UnsupportedOperationException("ERROR! Item " + nameInput + " is not available.");
            }

            // determine whether valid priority value entered
            if (!priorityInput.toString().matches("[0-7]")) {
                throw new UnsupportedOperationException("ERROR! Priority must be between 0-7.");
            }

           // if cart already contains item with conflicting priority
           for (int i = 0; i < cart.length(); i++) {
               CartItem cartItem = (CartItem)cart.getCartItems()[i];
               if (cartItem.getPriority().equals(priorityInput)){
                   throw new UnsupportedOperationException("ERROR! Priority conflicts with another in cart.");
               }
           }

           // create item for shopping cart
           CartItem newItem = new CartItem( nameInput, priorityInput, store );

           // add item to cart
           cart.add( newItem, store );

           // remove item from catalog
           store.removeFromStock( newItem.getItem() );

        }
        catch(UnsupportedOperationException e){
            System.out.println(e.getMessage());
        }
    }


    private void removeFromCart( ShoppingCart cart, Catalog store ){
        Scanner sc = new Scanner(System.in);
        System.out.print("Which item would you like to remove from the cart: ");
        String nameInput = sc.nextLine();
        try{
            for (int i = 0; i < cart.length(); i++) {

                // stop if cart contains nothing
                if( cart.length() == GroceryStoreConstants.MIN_CART_SIZE ){
                    throw new UnsupportedOperationException("ERROR! Can't remove any more items.");
                }

                // determine whether the cart contains the item
                CartItem cartItem = (CartItem)cart.getCartItems()[i];
                if( cartItem.getItem().getName().equals(nameInput)){
                    cart.removeFromCart(cartItem, store);
                }
            }
        }
        catch(UnsupportedOperationException e){
            System.out.println(e.getMessage());
        }
    }
}
