package Network;

import java.util.*;

public class Network {


    // INITIALIZE VARIABLES
    private Graph graph;
    private Scanner input = new Scanner(System.in);
    private String nodeLabels;




    public Network() throws Exception {


        // GET NETWORK SIZE
        Integer inputNetworkSize = getNetworkSize();  // Prompt user for network size

        // BUILD NETWORK
        setNetwork(inputNetworkSize);

        // GET/SET SOURCE + DESTINATION NODES
        Node[] srcDstNodes = getSrcAndDstNodes();  // Prompt user for source and destination Nodes

        // CALCULATE RESULTS
        Result result = shortestPathDijkstra(srcDstNodes[0], srcDstNodes[1], graph.getGraph(), nodeLabels.length() );


        // PRINT RESULT
        Console.println("ALL NODES: " + nodeLabels);
        Console.println("SRC NODE: " + srcDstNodes[0].getNodeName());
        Console.println("DST NODE: " + srcDstNodes[1].getNodeName());
        Console.println("FINAL PATH COST: " + result.totalCost);
        Console.print("SHORTEST PATH: " );
        for(Node n: result.shortestPathNodes){

            // If iterator reached last element in array, don't print arrow ( -> )
            if(result.shortestPathNodes.indexOf(n) == result.shortestPathNodes.size() -1){
                Console.print(n.getNodeName());
                break;
            }
            // Print each node name followed by an arrow to indicate next node in path
            Console.print(n.getNodeName() + " -> ");
        }

    }



    public Node[] getSrcAndDstNodes(){

        Node[] srcDstNodes = new Node[2];

        Console.print("Enter starting Node [A-Z]:");
        String inputSrcNode = input.nextLine();

        Console.print("Enter ending Node [A-Z]: ");
        String inputDstNode = input.nextLine();

        srcDstNodes[0] = graph.getNode(inputSrcNode);
        srcDstNodes[1] = graph.getNode(inputDstNode);

        return srcDstNodes;
    }



    public Integer getNetworkSize() throws Exception {

        Console.print("Enter Network Size [6,26]:");
        String inputNetworkSize = input.nextLine();

        if (inputNetworkSize.matches("(6)") || inputNetworkSize.matches("(26)")){
            Console.debug("User entered network size " + inputNetworkSize);
            return Integer.parseInt(inputNetworkSize);
        }
        throw new Exception("Invalid input.");
    }



    private void setNetwork(Integer size){
        try {

            String distanceMatrixFilename;

            if(size == 26){
                distanceMatrixFilename = "/Users/seansica/Documents/Boston University/CS-232 Programming with Java/Final Project/src/Network/distanceMatrix26x26";
                nodeLabels = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                graph = new Graph(size, distanceMatrixFilename, nodeLabels);

                Console.println("Network size set to " + size);
                Console.println("Graph created from distance matrix: " + distanceMatrixFilename);
                Console.println("Nodes created: " + distanceMatrixFilename);
            }

            if(size == 6){
                distanceMatrixFilename = "/Users/seansica/Documents/Boston University/CS-232 Programming with Java/Final Project/src/Network/distanceMatrix6x6";
                nodeLabels = "ABCDEF";
                graph = new Graph(size, distanceMatrixFilename, nodeLabels);

                Console.println("Network size set to " + size);
                Console.println("Graph created from distance matrix: " + distanceMatrixFilename);
                Console.println("Nodes created: " + distanceMatrixFilename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Result shortestPathDijkstra(Node src, Node dst, Edge[][] matrix, int size){

        /*
        WHILE nodes remain unvisited
            Visit unvisited Node with smallest known distance from starting Node (call this 'current node')
            FOR EACH unvisited neighbor of the current Node
                Calculate the cost/distance from starting Node
                IF the calculated cost/distance of this Node is less than the known cost/distance
                    Update shortest cost/distance to this Node
                    Update the previous Node with the current Node
                END IF
            NEXT unvisited neighbor
            Add the current Node to the list of visited Nodes
        END WHILE
         */


        // CHECK PARAMETERS
        Console.debug("Calculating Shortest Path from Node " + src.getNodeName() + " to Node " + dst.getNodeName());

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                Console.debug("Edge " + matrix[i][j].getEdgeName() + ", Cost " + matrix[i][j].getCost());
            }
        }

        int[] distance = new int[size];
        int[] previous = new int[size];

        // Get Index of Src and Dst Node
        int start = nodeLabels.indexOf(src.getNodeName());
        int end = nodeLabels.indexOf(dst.getNodeName());

        // HashSet used to track visited neighbors.
        // Duplicate (already visited) neighbors cannot be added to the HashSet.
        HashSet<Integer> Q = new HashSet<Integer>();

        // FIND STARTING NODE
        for (int i = 0; i < size; i++)
        {
            if (i == start) {
                Console.debug("Found starting node!");
                distance[start] = 0;
            }
            // SET COST FOR ALL UN-VISITED AND NON-STARTING NODES TO INFINITY
            else{
                /*
                 * IF starting edge cost >= 0
                 * THEN set current distance to cost of starting edge
                 * ELSE set current distance to infinity
                 */
                distance[i] = (matrix[start][i].getCost() >= 0) ? matrix[start][i].getCost() : Integer.MAX_VALUE;
                /*
                 * IF starting edge cost >= 0
                 * THEN set previous distance to current distance
                 * ELSE set previous distance to -1 (remove edge)
                 */
                previous[i] = (matrix[start][i].getCost() >= 0) ? start : -1;
                // Add current/starting edge to list of visited neighbors
                Q.add(i);
            }
        }

        // WHILE nodes remain unvisited
        while (Q.size() > 0){

            Console.debug("HashSet Size = " + Q.size());
            // find vertex with min distance
            // Set cost to infinity
            int u = Integer.MAX_VALUE;
            boolean first = true;
            for (int i : Q)
            {
                if (first)
                {
                    u = i;
                    first = false;
                }
                if (distance[i] < distance[u])
                    u = i;
            }
            if (u == end){

                Console.debug("Reached end Node!");

                int current = u;
                // Build List of Shortest Path Nodes
                ArrayList<Integer> path = new ArrayList<Integer>();
                path.add(current);
                while (current != start){
                    current = previous[current];
                    path.add(current);
                }
                Collections.reverse(path);
                //path.forEach(System.out::println);
                return new Result(path, distance[u], graph.getNodes());
            }
            Q.remove(u);

            // for each neighbor
            for (int v = 0; v < size; v++)
            {
                // if neighbor
                if (matrix[u][v].getCost() >= 0)
                {
                    int alt = distance[u] + matrix[u][v].getCost();
                    if (alt < distance[v])
                    {
                        distance[v] = alt;
                        previous[v] = u;
                    }
                }
            }
        }

        return null;
    }



}
