package Network;

public class Node {

    private String nodeName;
    private Edge[] edges;  // Maybe used for future expansion?
    private boolean visited;  // Maybe used for future expansion?


    /**
     * This is the constructor method for the Node class.
     * @param id This is a unique identifier that represents the specified node.
     */
    public Node(String id){
        super();
        this.nodeName = id;  // Set unique node identifier
    }



    /**
     * This is an access method that returns class attribute nodeName
     * @return This is the unique identifier for the specified Node
     */
    public String getNodeName(){
        return this.nodeName;
    }



    /**
     * This is an access method that returns class attribute neighbors
     * @return This is an ArrayList of all Nodes which are connected to the specified Node
     */
    public Edge[] getNeighbors(){
        if (edges == null) {
            throw new IllegalArgumentException("ERROR! Node has no Edges.");
        }
        return edges;
    }


    public Edge getLeastCostNeighbor(){

        Edge currentLowestCostEdge = edges[0];

        for (int i = 0; i < edges.length; i++) {

            // next number is higher than current number; found new highest
            if(currentLowestCostEdge.getCost() > edges[i].getCost()){
                currentLowestCostEdge = edges[i];
            }
        }
        return currentLowestCostEdge;
    }



    /**
     * This is a mutator method that changes the class attribute, nodeName
     * @param newName This is the new unique identifier that will be set in the specified Node
     */
    public void setNodeName(String newName){
        this.nodeName = newName;
    }

}
