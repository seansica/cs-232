package Network;

import java.util.ArrayList;

public class Result {


    ArrayList<Integer> shortestPathIndices;
    int totalCost;
    ArrayList<Node> shortestPathNodes;


    /**
     * This method represents the results of the SPF algorithm. It includes the total sum of path costs as well as a list of the corresponding Nodes in that path
     * @param shortestPathIndices Represents the index numbers of the Nodes specified by the SPF algorithm. The indices refer to their location in the global Nodes list
     * @param totalCost Represents the sum of all path costs calculated by the SPF algorithm
     * @param nodes Represents a list of all possible Nodes to which we should reference our index numbers
     */
    Result(ArrayList<Integer> shortestPathIndices, int totalCost, Node[] nodes)
    {
        this.shortestPathIndices = shortestPathIndices;
        this.totalCost = totalCost;
        this.shortestPathNodes = mapIndicesToNodeNames(shortestPathIndices, nodes);
    }

    /**
     * This method converts a list of Node indices discovered by the SPF algorithm to a list of corresponding Nodes.
     * @param path Represents the indices of the desired Nodes
     * @param nodes Represents a list of all possible Nodes to which we should reference our index numbers
     * @return List of Nodes discovered by the SPF algorithm
     */
    public ArrayList<Node> mapIndicesToNodeNames(ArrayList<Integer> path, Node[] nodes){


        ArrayList<Node> nodeMapping = new ArrayList<Node>();

        // Iterate over all given index numbers
        for (Integer index: path){

            // Iterate over all Nodes in network
            for (int i = 0; i < nodes.length; i++) {

                // If given Index number matches index number of Node in network
                if(i == index){

                    // Then match found: Add Node to mapping of Nodes to be returned to Result
                    nodeMapping.add(nodes[i]);
                }
            }
        }

        return nodeMapping;
    }
}

