package Network;

import java.util.ArrayList;
import java.util.List;

public class Edge{


    private String edgeName;  // Description of connected Nodes, i.e. AB, AC, AD, BC, etc.
    private ArrayList<Node> connectedNodes;  // list of connected Nodes
    private int cost;  // represents the cost of traversing this path


    /**
     * Default constructor
     * @param edgeName Specifies the name of the Edge
     * @param cost Specifies the cost or weight of the edge
     * @param varArgs Specifies that an unlimited number of Nodes may be passed as members of this Edge
     */
    public Edge(String edgeName, int cost, Node... varArgs){
        this.edgeName = edgeName;
        this.cost = cost;
        this.connectedNodes = new ArrayList<>();
        for (Node n: varArgs)
            this.connectedNodes.add(n);
    }

    public Edge(int cost){
        this.cost = cost;
        this.connectedNodes = new ArrayList<>();
    }


    public List<Node> getNodes(){
        return this.connectedNodes;
    }

    public String getEdgeName(){
        return this.edgeName;
    }


    public int getCost(){
        return this.cost;
    }

    public void setEdgeName(String newName){
        this.edgeName = newName;
    }


    public void addNode(Node node){
        this.connectedNodes.add(node);
    }


    public void setCost(int newCost){

        this.cost = newCost;
    }


}
