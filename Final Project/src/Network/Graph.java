package Network;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;



public class Graph{


    private Edge[][] adjMatrix;  // Represents the weighted distance matrix. Gets populated by given text file.
    private Node[] nodes;  // Represents list of Nodes connected in the distance matrix. Gets populated based on cardinality of distance matrix.



    /**
     * This is the default constructor that initializes the network of Nodes and Edges.
     */
    public Graph(Integer networkSize, String distanceMatrixFilename, String nodeLabels) throws FileNotFoundException {


        // Load distance matrix from given file
        File distanceMatrixFile = new File(distanceMatrixFilename);
        Scanner in = new Scanner(distanceMatrixFile);


        // Create VxV 2D Array
        adjMatrix = new Edge[networkSize][networkSize];


        /*
         * Create Array of Nodes
         */
        int numOfNodes = nodeLabels.length();  // Determine number of Nodes equal to length of given Node names list

        Console.error("nodeLabels.length()=" + nodeLabels.length());

        nodes = new Node[numOfNodes];  // Initialize ArrayList of Nodes

        char[] nodeNames = nodeLabels.toCharArray();  // Pre-stage Node names


        for (int i = 0; i < nodeNames.length; i++) {

            // Initialize one new Node for every pre-staged Node name
            Node n = new Node(String.valueOf(nodeNames[i]));


            // Add Node to ArrayList of Nodes
            nodes[i] = n;

            Console.debug("Created Node: " + nodes[i].getNodeName());
        }



        /*
         * Create iterator equal to number of Nodes -1.
         * Iterate one less time (V-1) because last index is zero (garbage value).
         * This will represent the row index.
         */
        for (int i = 0; i < networkSize; i++)
        {

            // Read in each line of given distanceMatrix file
            String line = in.nextLine();
            Console.debug("ROW: " + line);

            // For each line, define number of columns by the number of comma separators
            String[] columns = line.split(",");


            /*
             * Create nested iterator equal to number of Nodes -1.
             * Iterate one less time (V-1) because last index is zero (garbage value).
             * This will represent the column index.
             */
            for (int j = 0; j < networkSize; j++){

                // Capture cost
                int cost = Integer.parseInt(columns[j]);

                /*
                 * Create Edge with given cost and pointers to two Nodes
                 */

                // Step 1. Create Edge name based on member Nodes, i.e. AB, AC, AD, etc.
                String edgeName = nodes[i].getNodeName() + nodes[j].getNodeName();

                // Step 2. Create Edge based on cross-section result of two Nodes, their path cost, and Edge name. Populate into corresponding matrix.
                adjMatrix[i][j] = new Edge(edgeName, cost, nodes[i], nodes[j]);

                Console.debug("Created Edge: " + adjMatrix[i][j].getEdgeName());

                Console.debug("Edge " + String.valueOf(i) + "x" + String.valueOf(j) +
                        " connected to Node " + nodes[i].getNodeName() +
                        " with path cost " + adjMatrix[i][j].getCost());

                Console.debug("Edge " + String.valueOf(i) + "x" + String.valueOf(j) +
                        " connected to Node " + nodes[j].getNodeName() +
                        " with path cost " + adjMatrix[i][j].getCost());

                Console.debug("Node " + nodes[i].getNodeName() + " connected to Edge " +
                        adjMatrix[i][j].getEdgeName());
                }
        }
        in.close();  // Close scanner
    }


    /**
     * This method returns a Node from the 2D Matrix based on the specified Node name
     * @param nodeName  This is the specified Node name which will be searched for in the adjacency matrix
     * @return Node that matches specified Node name (String)
     */
    public Node getNode(String nodeName){

        try {
            for (int i = 0; i < nodes.length; i++) {
                if(nodes[i].getNodeName().toLowerCase().equals(nodeName.toLowerCase())){

                    Console.debug("Found Node " + nodes[i].getNodeName() + " that matches specified String " + nodeName);
                    return nodes[i];
                }
            }

            throw new NullPointerException("Node not found.");
        }
        catch(Exception e) {
            Console.error(e.getMessage());
            return null;
        }
    }

    /**
     * Getter method that provides access to the adjacency matrix
     * @return The adjacency matrix that represents a simulated network of nodes interconnected by weighted edges
     */
    public Edge[][] getGraph(){
        return this.adjMatrix;
    }

    /**
     * Getter method that provides access to the list of all Nodes included in the simulated network
     * @return Array of Nodes which are included in the simulated network
     */
    public Node[] getNodes(){
        return this.nodes;
    }
}
