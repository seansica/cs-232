# Network Shortest Path First

This program emulates a weighted network of nodes based on a given distance matrix and determines the shortest path from any specified source Node to any specified destination Node. It leverages Dijkstra's algorithm to compute the shortest path - meaning the path with the least weight -- to reach a given destination.

# Features
- Supports two different distance matrices: 6x6 and 26x26! The program prompts the user which matrix to use at launch. Additional distance matrices can easily be plugged in as well.
- The program allows the user to specify which source and destination nodes should be used for the SPF computation. The user is prompted at launch.
- Supports toggling of colored DEBUG (blue) and ERROR (red) messages in the console for additional verbose information.

# Files
All Class files are located in **src/Network**
- **Main**
    - A network is simulated inside of Main.
- **Network**
    - The Network class generates a weighted graph of Nodes and Edges based on the user-specified criteria, then calculates the shortest path.
- **Console**
    - This is an abstract class which centrally handles all console output for the program.
    - _Console.debug(String)_ and _Console.error(String)_ can be toggled on/off in the program. They also print text in Blue and Red, respectively.
- **Graph**
    - A two-dimensional weighted adjacency matrix is generated based on a given distance matrix (from file). The cardinality of the matrix determines the number of Nodes which should be generated. An Edge is generated for every cell in the matrix (equal to the matrix cardinality squared).
- **Node**
    - Represents a logical object which retains at least one Edge with another Node.
- **Edge**
    - Represents a logical path between at least two Nodes. The path also has an Integer path, which represents the weight or cost it takes for either Node to cross the Edge. A higher cost is considered less desirable.
- **Result**
    - The network calculates the shortest path to reach the destination nodes and stores the results in a special Result container. The container includes the total sum of path costs to reach the destination Node, as well as the Nodes contained in that path.

Please feel free to PM me if you find any bugs.

## Class Relationships UML
![UML](diagrams/uml_light.png)